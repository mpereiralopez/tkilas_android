package com.tksolutions.tkilas;


import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.tksolutions.bean.Local;
import com.tksolutions.util.Constants;
import com.tksolutions.util.LocationHelper;
import com.tksolutions.util.PlacesAutoCompleteAdapter;
import com.tksolutions.util.SuggestionPlaceAdapterItem;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


public class SearchActivity extends Activity{

    private String LOG_TAG = SearchActivity.class.getCanonicalName();

	//PASAR VALORES 1
    private int numBoton = 0;

	//SUMAR RESTAR PERSONAS
    int res = 0;
    private EditText n1;
    private int num1=1;

	//TIME
    private Button date;
    private Button time;

    private int hour;
    private int minute;
    private int day;
    private int dayOfMonth;
    private int month;


	//INTENT A PANTALLA 3
		//direccion
    AutoCompleteTextView mActvAddress;
    //hora
    Button dayInsert;
    //fecha
    Button timeInsert;
    //cantidad
    EditText cantidadInsert;

    private String mLatitude, mLongitude;

    private Calendar mTime;

    //Layout elements
    private CheckBox mChkNoTime;
    private TextView mTvWhen;
    private ImageView mImgPeople;
    private ImageButton mImgBtnNow, mBtnHome;
    private Button mBtnDatePicker, mBtnTimePicker;
    private EditText mInTxtPeople;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity_layout);

        this.numBoton = getIntent().getIntExtra(MainActivity.NUM_BOTON, 0);

        // Creates the actionbar with the custom layout
        setupActionBar();

        this.mChkNoTime = (CheckBox)findViewById(R.id.chk_noDate);
        this.mTvWhen = (TextView)findViewById(R.id.textView2_2);
        this.mImgPeople = (ImageView)findViewById(R.id.imageView1);
        this.mImgBtnNow = (ImageButton)findViewById(R.id.imageButton2);
        this.mInTxtPeople = (EditText)findViewById(R.id.cantidadInsert);
        this.mBtnDatePicker = (Button)findViewById(R.id.date);
        this.mBtnTimePicker = (Button)findViewById(R.id.time);
        this.mActvAddress = (AutoCompleteTextView)findViewById(R.id.direccionInsert);

        //TIMEPICKER
        setCurrentTimeOnView();
		setupDateTimePickers();

		//INTENT TODOS LOS DATOS A ACTIVITY 3

		dayInsert = (Button)findViewById(R.id.date);
		cantidadInsert = (EditText)findViewById(R.id.cantidadInsert);
		timeInsert = (Button)findViewById(R.id.time);


        //Disable controls for picking date when checkbox "no time" is checked.
        //Otherwise, they remain enabled.

        this.mChkNoTime.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                date.setEnabled(!isChecked);
                time.setEnabled(!isChecked);
                mTvWhen.setEnabled(!isChecked);
                mImgPeople.setEnabled(!isChecked);
                mImgBtnNow.setEnabled(!isChecked);

            }
        });

        boolean isHackNeeded = Build.VERSION.SDK_INT < 17;

        if (isHackNeeded) {
            final float scale = this.getResources().getDisplayMetrics().density;
            this.mChkNoTime.setPadding(this.mChkNoTime.getPaddingLeft() + (int)(20.0f * scale + 0.5f),
                    this.mChkNoTime.getPaddingTop(),
                    this.mChkNoTime.getPaddingRight(),
                    this.mChkNoTime.getPaddingBottom());
        }

        this.mActvAddress.setAdapter(new PlacesAutoCompleteAdapter(this, R.layout.list_item_places_autocomplete));
        this.mActvAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                SuggestionPlaceAdapterItem item = ((SuggestionPlaceAdapterItem)adapterView.getItemAtPosition(position));
                String str = item.getName();
                mLatitude = ""+item.getLatitude();
                mLongitude = ""+item.getLongitude();
                mActvAddress.setText(str);
                // Hide keyboard
                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(mActvAddress.getWindowToken(), 0);
            }
        });

        this.mImgBtnNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setCurrentTimeOnView();
            }
        });


	}

    /**
     * Sets the actionbar appearance
     */
    private void setupActionBar(){
        //Creates the ActionBar custom view from its custom layout
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mActionBarCustomView = mInflater.inflate(R.layout.custom_action_bar_ly, null);

        this.mBtnHome = (ImageButton)mActionBarCustomView.findViewById(R.id.btn_drawer);

        this.mBtnHome.setImageResource(R.drawable.home1x);

        this.mBtnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 =  new Intent(SearchActivity.this, MainActivity.class);
                startActivity(intent2);
            }
        });

        //Set actionbar features to enabling custom view and hiding app icon
        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(false);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayUseLogoEnabled(false);
        mActionBar.setCustomView(mActionBarCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);



    }


    /**
     * Action method for adding people button
     * @param view
     */
	public void Sum(View view){
		n1= (EditText) findViewById(R.id.cantidadInsert);

		//Extraigo el texto y lo convierto a int
		String uno = n1.getText().toString();
		int valorInicial=Integer.parseInt(uno);

		//realizo la suma
		int sum=valorInicial+num1;
		//Convierto el resultado a string
		String valorFinal = String.valueOf(sum);
		//muestro el valor
		n1.destroyDrawingCache();
		n1.setText(valorFinal);
	}

    /**
     * Action method for substracting people button
     * @param view
     */
	public void Res(View view){
		n1= (EditText) findViewById(R.id.cantidadInsert);

		//Extraigo el texto y lo convierto a int
		String uno = n1.getText().toString();
		int valorInicial=Integer.parseInt(uno);
		if(valorInicial<=1)
			valorInicial =1;
		else{
			//realizo la resta
			res=valorInicial-num1;


			//Convierto el resultado a string
			String valorFinal = String.valueOf(res);
			//muestro el valor
			n1.destroyDrawingCache();

			n1.setText(valorFinal);
		}
	}

    /**
     * Fill date & time fields with current values
     */
	public void setCurrentTimeOnView() {

		time = (Button) findViewById(R.id.time);
		date = (Button) findViewById(R.id.date);

		final Calendar c = Calendar.getInstance();
		hour = c.get(Calendar.HOUR_OF_DAY);
		minute = c.get(Calendar.MINUTE);
		dayOfMonth = c.get(Calendar.DAY_OF_MONTH);
		day = c.get(Calendar.DAY_OF_WEEK);
		month = c.get(Calendar.MONTH)+1;
		// cambio de media en media hora
        if(minute<30)
            minute=30;
        else{
            minute=00;
            hour=hour+1;
        }
		date.setText(new StringBuilder().append(pad(dayOfMonth)).append("/").append(pad(month)));

        String mSelectedDate = new StringBuilder().append(c.get(Calendar.YEAR)).append("-")
                .append(pad(month)).append("-").append(pad(dayOfMonth)).toString();

        ((TextView)findViewById(R.id.selectedDate)).setText(mSelectedDate);

        time.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)));
	}


    /**
     * TODO
     *
     */
	private void setupDateTimePickers() {

        this.mBtnDatePicker.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "datePicker");

            }
        });

		this.mBtnTimePicker.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
                DialogFragment newFragment = new TimePickerFragment();
                newFragment.show(getFragmentManager(), "timePicker");

            }
		});


	}

	private static String pad(int c) {
		if (c >= 10)
		   return String.valueOf(c);
		else
		   return "0" + String.valueOf(c);
	}


    /**
     * Action method for search button. Validates search form and execute search task
     * @param view
     */
	public void search(View view){
        if (this.validateForm()){
            (new SearchAsyncTask(SearchActivity.this)).execute();
        }

	}

    private boolean validateForm() {
        if (mActvAddress.getText().length() == 0){
            mActvAddress.setError(getString(R.string.empty_field_err));
            return false;
        }
        return true;

    }

    public void getCurrentLocationCoordinates(View view){
        LocationHelper locationHelper = new LocationHelper();
        final ProgressDialog mProgress = new ProgressDialog(this);
        mProgress.setMessage("Buscando localización");
        mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgress.setCancelable(true);
        mProgress.show();

        boolean locationEnabled = locationHelper.getLocation(view.getContext(), new LocationHelper.LocationResult() {
            @Override
            public void gotLocation(final Location location) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        printCurrentLocation(location);
                        mProgress.dismiss();
                    }
                });

            }
        });

        if (!locationEnabled){
            mProgress.dismiss();
            Toast.makeText(this, R.string.no_loc_system, Toast.LENGTH_LONG).show();

        }
    }


    /**
     * Look for the address for a given location
     * @param location
     */
    private void printCurrentLocation(Location location){
        if (location != null){

            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses;
            try {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (!addresses.isEmpty()){
                    final Address address = addresses.get(0);

                            // This code will always run on the UI thread, therefore is safe to modify UI elements.
                            mActvAddress.setText(address.getAddressLine(0));

                    mLatitude = ""+location.getLatitude();
                    mLongitude = ""+location.getLongitude();

                }else {
                    Toast.makeText(this, R.string.error_getting_location,Toast.LENGTH_LONG).show();
                }

            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), R.string.error_resolving_address,Toast.LENGTH_LONG).show();
            }

        }else{
            Toast.makeText(this, R.string.error_getting_location,Toast.LENGTH_LONG).show();
        }


    }







    /***************************** DateTime pickers fragments*************************/

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Display the picked time in the button
            Button mHolder = (Button)getActivity().findViewById(R.id.time);
            // Round the time according to 30min fractions
            if(minute<=30)
                minute=30;
            else{
                minute=00;
                hourOfDay=hourOfDay+1;
            }
            mHolder.setText(new StringBuilder().append(pad(hourOfDay)).append(":").append(pad(minute)));

        }
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog picker = new DatePickerDialog(getActivity(), this, year, month, day);
            picker.getDatePicker().setMinDate(c.getTimeInMillis());
            return picker;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Display the picked date in the button
            String buttonText = new StringBuilder().append(pad(day)).append("/").append(pad(month+1))
                            .toString();
            String mTextDate = new StringBuilder().append(year).append("-").append(pad(month+1))
                            .append("-").append(pad(day)).toString();

            ((TextView)getActivity().findViewById(R.id.selectedDate)).setText(mTextDate);
            Button mHolder = (Button)getActivity().findViewById(R.id.date);
            mHolder.setText(buttonText);
        }
    }


    /**
     * Private class to consume search locals WS
     */
    private class SearchAsyncTask extends AsyncTask<Void, Void, ArrayList<Local>>{

        private ProgressDialog mProgressDialog;
        private Context mContext;
        private Activity mActivity;
        private String positionAddress, selectedDate, selectedTime;

        public SearchAsyncTask(Activity activity){
            super();
            this.mActivity = activity;
            this.mContext = activity.getApplicationContext();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(mActivity);
            mProgressDialog.setMessage(getString(R.string.search_progress));
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();



        }

        @Override
        protected ArrayList<Local> doInBackground(Void... voids) {

            positionAddress = mActvAddress.getText().toString();
            selectedDate = ((TextView)mActivity.findViewById(R.id.selectedDate))
                                .getText().toString();

            selectedTime = ((Button)mActivity.findViewById(R.id.time))
                    .getText().toString();

            // Create the login URL with email and password GET parameters
            URI targetUrl= UriComponentsBuilder.fromUriString(Constants.SEARCH_URL)
                    .queryParam(Constants.KEY_LATITUD, mLatitude)
                    .queryParam(Constants.KEY_LONGITUD, mLongitude)
                    .queryParam("specificDate", selectedDate)
                    .build()
                    .toUri();


            // Set custom header: API_KEY
            HttpHeaders headers = new HttpHeaders();
            headers.add("API_KEY", Constants.TKILAS_API_KEY);
            HttpEntity<Object> entity = new HttpEntity<Object>(headers);


            // Set Jackson Json converter
            List converters = new ArrayList<HttpMessageConverter<?>>();
            converters.add(new MappingJackson2HttpMessageConverter());

            // Create Spring Rest Template and invoke REST WS
            RestTemplate template = new RestTemplate();
            template.setMessageConverters(converters);
            Local[] results = template.exchange(targetUrl, HttpMethod.GET, entity, Local[].class).getBody();


            ArrayList<Local> arrayListResult = new ArrayList<Local>(Arrays.asList(results));

            // Filtado por bebida seleccionada
            // Compara la bebida con los precios de bebidas
            Iterator<Local> iter = arrayListResult.iterator();

            while (iter.hasNext()) {
                Local local = iter.next();
                if (numBoton == 1 && local.getCostBeer() == 0.0) {
                    iter.remove();

                } else if (numBoton == 2 && local.getCostCoffee() == 0.0) {
                    iter.remove();

                    //drink
                } else if (numBoton == 3 && local.getCostCocktail() == 0.0) {
                    iter.remove();

                    //bottle
                } else if (numBoton == 4 && local.getCostAlcBottel() == 0.0) {
                    iter.remove();

                }
            }

            return arrayListResult;
        }


        @Override
        protected void onPostExecute(ArrayList<Local> results) {

            this.mProgressDialog.dismiss();



            ResultsActivity.data = results;
            Intent intent = new Intent(mActivity, ResultsActivity.class);
            intent.putExtra(MainActivity.NUM_BOTON, numBoton);
            intent.putExtra(Constants.KEY_LATITUDE, mLatitude);
            intent.putExtra(Constants.KEY_LONGITUDE, mLongitude);
            intent.putExtra(Constants.KEY_PEOPLE, Integer.parseInt(mInTxtPeople.getText().toString()));
            if ( !mChkNoTime.isChecked() ){
                String strTime[] = selectedTime.split(":");
                String strDate[] = selectedDate.split("-");

                Calendar date = Calendar.getInstance();
                date.set(Calendar.DAY_OF_MONTH, Integer.parseInt(strDate[2]));
                date.set(Calendar.MONTH, Integer.parseInt(strDate[1])-1);
                date.set(Calendar.YEAR, Integer.parseInt(strDate[0]));
                date.set(Calendar.HOUR_OF_DAY, Integer.parseInt(strTime[0]));
                date.set(Calendar.MINUTE, Integer.parseInt(strTime[1]));

                intent.putExtra(Constants.KEY_DATETIME, date.getTimeInMillis());
            }

            mActivity.startActivity(intent);
        }
    }






}




