package com.tksolutions.tkilas;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.tksolutions.database.DaoTkilas;
import com.tksolutions.util.Constants;
import com.tksolutions.util.OfferHistoryItem;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class OfferConfirmationActivity extends Activity{
    String mUserId, mUserName;
    private ImageButton mBtnHome;
    private ImageView mImgLocal;
    private TextView mTxtInName, mTxtDate, mTxtTime, mTxtPeople, mTxtConditions;
    private Button mBtnConfirm, mBtnOfferTag;

    // Intent data
    private String mLocalName, mTagText, mDateText;
    private int mAction, mPeople, mPromoIndex;
    private long mDateTime;

	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offer_confirmation_layout);

        this.mAction = getIntent().getIntExtra(Constants.KEY_ACTION, -1);
        this.mTagText = getIntent().getStringExtra(Constants.KEY_TAG_TEXT);
        this.mLocalName = getIntent().getStringExtra(Constants.KEY_LOCAL_NAME);
        this.mDateTime = getIntent().getLongExtra("dateTime", 0);
        this.mPeople = getIntent().getIntExtra(Constants.KEY_PEOPLE, -1);
        this.mPromoIndex = getIntent().getIntExtra(Constants.KEY_PROMO_INDEX, -1);
        this.setupActionBar();

        SharedPreferences prefs = this.getSharedPreferences(Constants.PREFS_NAME, 0);
        mUserId = prefs.getString(Constants.USER_KEY,"");
        mUserName = prefs.getString(Constants.USER_NAME_KEY, "Invitado");

        this.mImgLocal = (ImageView)findViewById(R.id.local_image_confirm);
        this.mBtnOfferTag = (Button)findViewById(R.id.selected_offer);
        this.mBtnConfirm = (Button)findViewById(R.id.buttonConfirm);
        this.mTxtInName = (TextView)findViewById(R.id.tv_in_name);
        this.mTxtDate = (TextView)findViewById(R.id.confim_date);
        this.mTxtTime = (TextView)findViewById(R.id.confim_time);
        this.mTxtPeople = (TextView)findViewById(R.id.confirm_people);
        this.mTxtConditions = (TextView)findViewById(R.id.conditions);
        this.mTxtConditions.setText(R.string.aceptar_condiciones);

        /********* Build the screen regarding the action ******/
        // User name
        this.mTxtInName.setText(getString(R.string.offer_in_name, mUserName));
        // Tag text
        this.mBtnOfferTag.setText(mTagText);

        // Date & hour
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(mDateTime);
        String month = getResources().getStringArray(R.array.months)[cal.get(Calendar.MONTH)];
        this.mDateText = cal.get(Calendar.DAY_OF_MONTH) + getString(R.string.date_confirm, month);
        this.mTxtDate.setText(mDateText);
        int minute = cal.get(Calendar.MINUTE);
        int hour = cal.get(Calendar.HOUR_OF_DAY);

        this.mTxtTime.setText(pad(hour) + ":" + pad(minute));

        // People
        if (mPeople > 1){
            this.mTxtPeople.setText(getString(R.string.people_confirm, mPeople)+"s");
        } else{
            this.mTxtPeople.setText(getString(R.string.people_confirm, mPeople));
        }

        // Modify layout regarding the action
        if (mAction == Constants.REQUEST_CODE_OFFER_SHOW){
            this.mBtnConfirm.setVisibility(View.GONE);
            this.mTxtConditions.setVisibility(View.GONE);

        }else {
            this.mBtnConfirm.setVisibility(View.VISIBLE);
            this.mImgLocal.setImageBitmap(OfferDetailActivity.local.getPic1());
        }


	}

    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }


    /**
     * Sets the actionbar appearance
     */
    private void setupActionBar(){
        //Creates the ActionBar custom view from its custom layout
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mActionBarCustomView = mInflater.inflate(R.layout.custom_action_bar_ly, null);

        this.mBtnHome = (ImageButton)mActionBarCustomView.findViewById(R.id.btn_drawer);

        this.mBtnHome.setImageResource(R.drawable.back_button);

        this.mBtnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ((TextView)mActionBarCustomView.findViewById(R.id.tkilas_action_bar_title))
                .setText(mLocalName);

        //Set actionbar features to enabling custom view and hiding app icon
        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(false);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayUseLogoEnabled(false);
        mActionBar.setCustomView(mActionBarCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);


    }

    /**
     * Action performed when Confirm button is clicked
     * If there is credentials stored in preferences, call confirmation WS.
     * Otherwise, open login activity
     * @param v
     */
    public void confirmReservation(View v){
        SharedPreferences prefs = getSharedPreferences(Constants.PREFS_NAME, 0);
        String mUserId = prefs.getString(Constants.USER_KEY, "");
        String mAuth = prefs.getString(Constants.USER_AUTH_KEY, "");
        if (mUserId.isEmpty() || mAuth.isEmpty()){
            Intent intent = new Intent(OfferConfirmationActivity.this, LoginActivity.class);
            startActivityForResult(intent, Constants.REQUEST_CODE_LOGIN);
        } else {
            (new ConfirmReservationAsyncTask(this)).execute();
        }

    }


    private class ConfirmReservationAsyncTask extends AsyncTask<Void, Void, HttpStatus>{

        private ProgressDialog mProgressDialog;
        private Context mContext;
        private Activity mActivity;

        public ConfirmReservationAsyncTask(Activity activity){
            super();
            this.mActivity = activity;
            this.mContext = activity.getApplicationContext();
        }

        @Override
        protected void onPreExecute() {

            mProgressDialog = new ProgressDialog(mActivity);
            mProgressDialog.setMessage("Reservando....");
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();



        }


        @Override
        protected HttpStatus doInBackground(Void... voids) {

            SharedPreferences prefs = mContext.getSharedPreferences(Constants.PREFS_NAME, 0);
            String auth = prefs.getString(Constants.USER_AUTH_KEY,"");
            String mUserId = prefs.getString(Constants.USER_KEY, "");

            JsonNodeFactory factory = JsonNodeFactory.instance;
            ObjectNode root = factory.objectNode();
            ObjectNode idNode = factory.objectNode();


            // Build body request as Json
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(mDateTime);
            String dateAux = cal.get(Calendar.YEAR) + "-"
                    + (cal.get(Calendar.MONTH)+1)+ "-" +pad(cal.get(Calendar.DAY_OF_MONTH));
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            String time = format.format(new Date(cal.getTimeInMillis()));

            idNode.put(Constants.CONFIRM_KEYPARM_ID_CLIENT, Long.parseLong(mUserId));
            idNode.put(Constants.CONFIRM_KEYPARM_ID_LOCAL, Long.parseLong(OfferDetailActivity.local.getId()));
            idNode.put(Constants.CONFIRM_KEYPARM_DATE, dateAux);
            root.put(Constants.CONFIRM_KEYPARM_ID, idNode);
            root.put(Constants.CONFIRM_KEYPARM_SIZE, mPeople);
            root.put(Constants.CONFIRM_KEYPARM_HOUR, time);

            String url;
            if (mPromoIndex != -1){
                root.put(Constants.CONFIRM_KEYPARM_PROMO_INDX, mPromoIndex);
                url = Constants.CONFIRM_PROMO_URL;
            }else {
                url = Constants.CONFIRM_DISCOUNT_URL;
            }

            // Set custom header: API_KEY
            HttpHeaders headers = new HttpHeaders();
            headers.add(Constants.HEADER_API, Constants.TKILAS_API_KEY);
            headers.add(Constants.HEADER_AUTHORIZATION, "Basic " + auth);
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

            HttpEntity<ObjectNode> request = new HttpEntity<ObjectNode>(root, headers);

            // Set Jackson Json converter
            List converters = new ArrayList<HttpMessageConverter<?>>();
            converters.add(new MappingJackson2HttpMessageConverter());
            converters.add(new StringHttpMessageConverter(Charset.defaultCharset()));

            // Create Spring Rest Template and invoke REST WS
            RestTemplate template = new RestTemplate();
            template.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
            template.setMessageConverters(converters);

            HttpStatus responseCode;
            try{
                ResponseEntity<JsonNode> response = template.postForEntity(url, request, JsonNode.class);
                responseCode = response.getStatusCode();
            }catch (RestClientException e){
                responseCode = HttpStatus.INTERNAL_SERVER_ERROR;
            }



            return responseCode;
        }

        @Override
        protected void onPostExecute(HttpStatus status) {
            mProgressDialog.dismiss();

                switch (status) {

                    case CREATED:

                        OfferHistoryItem item = new OfferHistoryItem();

                        item.setIdUser(Integer.parseInt(mUserId));
                        item.setIdLocal(Integer.parseInt(OfferDetailActivity.local.getId()));
                        item.setLocalName(mLocalName);
                        item.setTagText(mTagText);
                        item.setDateText(mDateText);
                        item.setDateTime(mDateTime);
                        item.setNumPeople(mPeople);
                        item.setTimeText(mTxtTime.getText().toString());


                        // DB opening
                        DaoTkilas dao =
                                new DaoTkilas(mContext, Constants.DB_NAME, null, 1);

                        SQLiteDatabase db = dao.getWritableDatabase();

                        // Store the retrieved user into the local database
                        dao.insertOfferHistory(db, item);

                        db.close();

                        mBtnConfirm.setVisibility(View.GONE);
                        mTxtConditions.setText("¡Reserva realizada!");
                        mBtnHome.setImageResource(R.drawable.home1x);
                        mBtnHome.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(mActivity, MainActivity.class);
                                startActivity(intent);
                                mActivity.finish();

                            }
                        });
                        break;

                    case INTERNAL_SERVER_ERROR:

                        this.buildDialog(getString(R.string.server_error)).show();
                        Log.i("PostExecute", "SERVER_ERROR");
                        break;

                    default:
                        this.buildDialog(getString(R.string.unexpected_error)).show();
                }

            //Toast.makeText(mContext, "Reservado!", Toast.LENGTH_LONG).show();


        }

        private AlertDialog buildDialog(String message) {
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage(message)
                    .setTitle(R.string.reservation_dialog_title);

            builder.setCancelable(true);

            // 3. Get the AlertDialog from create()
            AlertDialog dialog = builder.create();

            dialog.setCanceledOnTouchOutside(true);

            return dialog;
        }
    }
}

