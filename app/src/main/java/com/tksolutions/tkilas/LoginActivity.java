package com.tksolutions.tkilas;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.tksolutions.bean.User;
import com.tksolutions.database.DaoTkilas;
import com.tksolutions.util.Constants;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;


public class LoginActivity extends Activity{
	// Layout elements

    private Button btnLogin, btnSignup;
    private EditText mInTxtEmail, mInTxtPassword;
    //private TextView loginErrorMsg;


    private ImageButton mBtnHome;
    
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_activity_layout);

        setupActionBar();

        this.btnLogin = (Button)findViewById(R.id.btn_login);
        this.btnSignup = (Button)findViewById(R.id.btn_signup);
        this.mInTxtEmail = (EditText)findViewById(R.id.et_login_email);
        this.mInTxtPassword = (EditText)findViewById(R.id.et_login_password);




        this.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validateForm()){
                    (new LoginAsyncTask(LoginActivity.this)).execute();
                }
            }
        });


        // Setup the actions for the register button
        this.btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                startActivity(intent);
                finish();

            }
        });

 
      
    }


    /**
     * Check whether email and password fields are filled
     * @return
     */
    private boolean validateForm() {
        boolean isValid = true;

        if (mInTxtEmail.getText().toString().trim().isEmpty()){
            mInTxtEmail.setError(getString(R.string.empty_field_err));
            isValid = false;
        }

        if (mInTxtPassword.getText().toString().trim().isEmpty()){
            mInTxtPassword.setError(getString(R.string.empty_field_err));
            isValid = false;
        }

        return isValid;

    }


    /**
     * Sets the actionbar appearance
     */
    private void setupActionBar(){
        //Creates the ActionBar custom view from its custom layout
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mActionBarCustomView = mInflater.inflate(R.layout.custom_action_bar_ly, null);

        this.mBtnHome = (ImageButton)mActionBarCustomView.findViewById(R.id.btn_drawer);

        this.mBtnHome.setImageResource(R.drawable.home1x);

        this.mBtnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 =  new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent2);
            }
        });


        ((TextView)mActionBarCustomView.findViewById(R.id.tkilas_action_bar_title))
                .setText(R.string.inicio_sesion);

        mActionBarCustomView.findViewById(R.id.tkilas_bar_literal).setVisibility(View.INVISIBLE);

        //Set actionbar features to enabling custom view and hiding app icon
        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(false);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayUseLogoEnabled(false);
        mActionBar.setCustomView(mActionBarCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);



    }

    @Override
    public void onBackPressed() {
        // Return cancelled to the caller activity
        setResult(Constants.RESULT_CANCELLED);
        finish();
    }

		
	//registro
		public void Inscribete(View view){
			Intent intent= null;		
			intent = new Intent(LoginActivity.this, SignUpActivity.class);
		    startActivity(intent);
		}



    private class LoginAsyncTask extends AsyncTask<Void, Void, User>{

        private Activity mActivity;
        private Context mContext;
        private ProgressDialog mProgressDialog;

        public LoginAsyncTask(Activity activity){
            super();
            this.mActivity = activity;
            this.mContext = activity.getApplicationContext();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(mActivity);
            mProgressDialog.setMessage(getString(R.string.login_progress));
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            if (!isFinishing()){
                mProgressDialog.show();
            }

        }

        @Override
        protected User doInBackground(Void... voids) {
            // Create the login URL with email and password GET parameters
            URI targetUrl= UriComponentsBuilder.fromUriString(Constants.LOGIN_URL)
                    .queryParam("email", mInTxtEmail.getText().toString().trim())
                    .queryParam("pass", mInTxtPassword.getText().toString().trim())
                    .build()
                    .toUri();


            // Set custom header: API_KEY
            HttpHeaders headers = new HttpHeaders();
            headers.add("API_KEY", Constants.TKILAS_API_KEY);
            HttpEntity<Object> entity = new HttpEntity<Object>(headers);


            // Set Jackson Json converter
            List converters = new ArrayList<HttpMessageConverter<?>>();
            converters.add(new MappingJackson2HttpMessageConverter());

            // Create Spring Rest Template and invoke REST WS
            RestTemplate template = new RestTemplate();
            template.setMessageConverters(converters);
            User user = null;
            try{
                user = template.exchange(targetUrl, HttpMethod.GET, entity, User.class).getBody();
            }catch (RestClientException e){
                e.printStackTrace();
            }

            return user;

        }

        @Override
        protected void onPostExecute(User user) {
            mProgressDialog.dismiss();
            if (user != null && user.getUserInfo() != null){
                // DB opening
                DaoTkilas dao =
                        new DaoTkilas(mContext, Constants.DB_NAME, null, 1);

                SQLiteDatabase db = dao.getWritableDatabase();

                // Store the retrieved user into the local database
                dao.insertUser(db, user);

                db.close();

                // Create Base64 auth value to save in Preferences.
                String credentials = user.getUserInfo().getEmail() + ":"
                        + user.getUserInfo().getPassword();

                String auth = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                // Store the user id into the application preferences.
                SharedPreferences prefs = mContext.getSharedPreferences(Constants.PREFS_NAME, 0);
                SharedPreferences.Editor edit = prefs.edit();
                edit.putString(Constants.USER_KEY, user.getUserInfo().getId());
                edit.putString(Constants.USER_AUTH_KEY, auth);
                edit.putString(Constants.USER_NAME_KEY, user.getUserInfo().getName());
                edit.putString(Constants.USER_EMAIL, user.getUserInfo().getEmail());
                edit.commit();


                // Return user id to the caller activity and OK code
                Intent intent = new Intent();
                intent.putExtra(Constants.USER_KEY, user.getUserInfo().getId());
                intent.putExtra(Constants.USER_NAME_KEY, user.getUserInfo().getName());
                mActivity.setResult(Constants.RESULT_OK, intent);
                finish();


            }else{
                // Show error message in Toast.
                Toast.makeText(mContext, R.string.login_ko, Toast.LENGTH_LONG).show();

            }


        }
    }
		
}
