package com.tksolutions.tkilas;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.tksolutions.database.DaoTkilas;
import com.tksolutions.util.Constants;

/**
 * Created by carlos on 5/11/14.
 */
public class ProfileActivity extends Activity {

    private ImageButton mBtnHome, mImgBtnProfilePic;
    private TextView mTxtNombre, mTxtEmail;

    private String mUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_layout);

        SharedPreferences prefs = this.getSharedPreferences(Constants.PREFS_NAME, 0);
        this.mUserId = prefs.getString(Constants.USER_KEY, "");


        this.mTxtNombre = (TextView)findViewById(R.id.txtNombre);
        this.mTxtEmail = (TextView)findViewById(R.id.txtEmail);
        this.mImgBtnProfilePic = (ImageButton)findViewById(R.id.profile_pic);

        this.mImgBtnProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent (Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(Intent.createChooser(photoPickerIntent,"Select Picture"), Constants.REQUEST_CODE_IMAGE_GALLERY);
            }
        });
        this.setupActionBar();

        mTxtNombre.setText(prefs.getString(Constants.USER_NAME_KEY, "Invitado"));
        String mEmail = prefs.getString(Constants.USER_EMAIL,"");
        this.mTxtEmail.setText(mEmail);
        if (this.mUserId != ""){
            DaoTkilas dao = new DaoTkilas(this, Constants.DB_NAME, null, 1);
            SQLiteDatabase db = dao.getReadableDatabase();

            byte byteArray[] = dao.loadProfilePic(db, this.mUserId);

            if (byteArray != null){
                this.mImgBtnProfilePic.setImageBitmap(BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length));
            }
        }


    }

    /**
     * Sets the actionbar appearance
     */
    private void setupActionBar(){
        //Creates the ActionBar custom view from its custom layout
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mActionBarCustomView = mInflater.inflate(R.layout.custom_action_bar_ly, null);

        this.mBtnHome = (ImageButton)mActionBarCustomView.findViewById(R.id.btn_drawer);

        this.mBtnHome.setImageResource(R.drawable.back_button);

        this.mBtnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ((TextView)mActionBarCustomView.findViewById(R.id.tkilas_action_bar_title))
                .setText("Mi perfil");

        //Set actionbar features to enabling custom view and hiding app icon
        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(false);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayUseLogoEnabled(false);
        mActionBar.setCustomView(mActionBarCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CODE_IMAGE_GALLERY){
            Uri selectedImageUri = data.getData();
            String selectedImagePath = getPath(selectedImageUri);

            DaoTkilas dao = new DaoTkilas(this, Constants.DB_NAME, null, 1);
            SQLiteDatabase db = dao.getWritableDatabase();

            Bitmap bmp = BitmapFactory.decodeFile(selectedImagePath);

            if ( dao.saveUserProfilePic(db, this.mUserId, bmp) ){
                this.mImgBtnProfilePic.setImageBitmap(bmp);

            }else{
                Toast.makeText(this, "Error actualizando la imagen de perfil", Toast.LENGTH_LONG).show();
            }

        }

    }


    public String getPath(Uri uri){

        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        return s;
    }
}
