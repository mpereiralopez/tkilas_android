package com.tksolutions.tkilas;


import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.app.ActionBar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tksolutions.util.Constants;

public class MainActivity extends Activity {

    //creo string statico para ver el boton pulsado
    public final static String MSG_BOTON_PULSADO = "app.tkilas1_1.SearchActivity.MSG_BOTON_PULSADO";
    public final static String NUM_BOTON = "app.tkilas1_1.SearchActivity.NUM_BOTON";

    private String mUserId;



    //prueba2
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    CustomDrawerAdapter adapter;
    List<DrawerItem> dataList;

    private ImageButton mBtnDrawer;
    private TextView mTvTitleQuestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        // Sets layout on this activity
        setContentView(R.layout.activity_main);

        // Creates the actionbar with the custom layout
        setupActionBar();


        this.mBtnDrawer = (ImageButton) getActionBar().getCustomView().findViewById(R.id.btn_drawer);
        this.mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        this.mTvTitleQuestion = (TextView)findViewById(R.id.textView1_1);

        Typeface typeface = Typeface.createFromAsset(getApplicationContext().getAssets(),"font/Aller_Rg.ttf");
        this.mTvTitleQuestion.setTypeface(typeface);

        setupNavigationDrawer(this.mBtnDrawer);


    }


    /**
     * Sets the actionbar appearance
     */
    private void setupActionBar(){
        //Creates the ActionBar custom view from its custom layout
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mActionBarCustomView = mInflater.inflate(R.layout.custom_action_bar_ly, null);

        //Set actionbar features to enabling custom view and hiding app icon
        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(false);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayUseLogoEnabled(false);
        mActionBar.setCustomView(mActionBarCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);



    }

    /**
     * Setup the navigation drawer toggle on the view got as a parameter
     * @param drawerToggle
     */
    private void setupNavigationDrawer(View drawerToggle){

        drawerToggle.setOnClickListener(new View.OnClickListener() {
            private boolean isDrawerOpened = false;
            @Override
            public void onClick(View view) {
                if (!isDrawerOpened){
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                    isDrawerOpened = true;
                }else{
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                    isDrawerOpened = false;
                }
            }
        });

        // Drawer setup
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        // Add Drawer Item to dataList
        dataList = new ArrayList<DrawerItem>();
        dataList.add(new DrawerItem(getString(R.string.drawer_option_1), R.drawable.ic_action_about));
        dataList.add(new DrawerItem(getString(R.string.drawer_option_2), R.drawable.ic_action_camera));
        adapter = new CustomDrawerAdapter(this, R.layout.custom_drawer_item, dataList);

        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                onDrawerItemSelected(position);
            }
        });
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String message = "";
        if (requestCode == Constants.REQUEST_CODE_LOGIN) {
            switch (resultCode) {
                case Constants.RESULT_OK:
                    this.mUserId = data.getStringExtra(Constants.USER_KEY);
                    String name = data.getStringExtra(Constants.USER_NAME_KEY);
                    message = getString(R.string.login_successful, name);
                    Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                    break;

                case Constants.RESULT_CANCELLED:
                    message = getString(R.string.cancelled);
                    Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                    break;
            }

        }



    }

    /**
     * Changes view between Perfil and Historial regarding the drawer selection
     * @param position
     */
    public void onDrawerItemSelected(int position) {

        SharedPreferences prefs = this.getSharedPreferences(Constants.PREFS_NAME, 0);
        String mUserId = prefs.getString(Constants.USER_KEY, "");
        if (mUserId == "") {
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivityForResult(intent, Constants.REQUEST_CODE_LOGIN);
        } else {

            switch (position) {
                case 0:

                    Intent intentProfile = new Intent(MainActivity.this, ProfileActivity.class);
                    startActivity(intentProfile);

                    break;
                case 1:
                    Intent intent = new Intent(MainActivity.this, HistoryActivity.class);
                    startActivity(intent);
                    break;
                default:break;
            }
        }

        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerList);

    }


    private AlertDialog buildDialog(String message) {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage(message)
                .setTitle(R.string.reservation_dialog_title);

        builder.setCancelable(true);

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();

        dialog.setCanceledOnTouchOutside(true);

        return dialog;
    }


    /*
     * _________________________________________________________________________________________________________________________
     */

    //botones a pantalla 2 y 3
    public void CallBtn(View view) {

        Intent intent = null;

        int id = view.getId();
        if (id == R.id.btn1_1) {
            intent = new Intent(MainActivity.this, SearchActivity.class);
            intent.putExtra(MSG_BOTON_PULSADO, "CERVEZA");
            intent.putExtra(NUM_BOTON, 1);
            startActivity(intent);
        } else if (id == R.id.btn1_2) {
            intent = new Intent(MainActivity.this, SearchActivity.class);
            intent.putExtra(MSG_BOTON_PULSADO, "CAF�");
            intent.putExtra(NUM_BOTON, 2);
            startActivity(intent);
        } else if (id == R.id.btn1_3) {
            intent = new Intent(MainActivity.this, SearchActivity.class);
            intent.putExtra(MSG_BOTON_PULSADO, "COPA");
            intent.putExtra(NUM_BOTON, 3);
            startActivity(intent);
        } else if (id == R.id.btn1_4) {
            intent = new Intent(MainActivity.this, SearchActivity.class);
            intent.putExtra(MSG_BOTON_PULSADO, "BOTELLA");
            intent.putExtra(NUM_BOTON, 4);
            startActivity(intent);
        } else if (id == R.id.btn1_5) {
            intent = new Intent(MainActivity.this, SearchActivity.class);
            intent.putExtra(MSG_BOTON_PULSADO, "TODO");
            intent.putExtra(NUM_BOTON, 5);
            startActivity(intent);
        } else if (id == R.id.btn1_6) {
            intent = new Intent(MainActivity.this, SearchActivity.class);
            intent.putExtra(MSG_BOTON_PULSADO, "HAS ELEGIDO POPULARES");
            //TODO intent.putExtra(NUM_BOTON, 6);
            intent.putExtra(NUM_BOTON,  5);
            startActivity(intent);
        } else if (id == R.id.btn1_7) {
            intent = new Intent(MainActivity.this, SearchActivity.class);
            intent.putExtra(MSG_BOTON_PULSADO, "HAS ELEGIDO DESCUENTOS");
            //TODO intent.putExtra(NUM_BOTON,  7);
            intent.putExtra(NUM_BOTON,  5);
            startActivity(intent);
        } else {
        }

    }






}
