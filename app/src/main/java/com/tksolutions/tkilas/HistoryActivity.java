package com.tksolutions.tkilas;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.tksolutions.database.DaoTkilas;
import com.tksolutions.util.Constants;
import com.tksolutions.util.OfferHistoryAdapter;
import com.tksolutions.util.OfferHistoryItem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class HistoryActivity extends Activity{

    private ImageButton mBtnHome;
    private ListView mLvHistory;

	 public void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.history_activity_layout);

         this.setupActionBar();

         this.mLvHistory = (ListView)findViewById(R.id.history_listview);

         TextView emptyText = (TextView)findViewById(R.id.history_empty);
         this.mLvHistory.setEmptyView(emptyText);


         DaoTkilas dao = new DaoTkilas(this, Constants.DB_NAME, null, 1);

         SQLiteDatabase db = dao.getReadableDatabase();

         List<OfferHistoryItem> historyItems =  dao.selectAllOfferHistory(db);

         OfferHistoryAdapter adapter = new OfferHistoryAdapter(this, historyItems);

         this.mLvHistory.setAdapter(adapter);

         this.mLvHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
             @Override
             public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                 Intent intent = new Intent(HistoryActivity.this, OfferConfirmationActivity.class);
                 OfferHistoryItem item = (OfferHistoryItem)adapterView.getItemAtPosition(pos);
                 intent.putExtra(Constants.KEY_ACTION, Constants.REQUEST_CODE_OFFER_SHOW);
                 intent.putExtra(Constants.KEY_PROMO_POS, pos);
                 intent.putExtra(Constants.KEY_TAG_TEXT, item.getTagText());
                 intent.putExtra(Constants.KEY_PEOPLE, item.getNumPeople());
                 intent.putExtra(Constants.KEY_DATETIME, item.getDateTime());
                 intent.putExtra(Constants.KEY_LOCAL_NAME, item.getLocalName());
                 startActivity(intent);

             }
         });



	 }




    /**
     * Sets the actionbar appearance
     */
    private void setupActionBar(){
        //Creates the ActionBar custom view from its custom layout
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mActionBarCustomView = mInflater.inflate(R.layout.custom_action_bar_ly, null);

        this.mBtnHome = (ImageButton)mActionBarCustomView.findViewById(R.id.btn_drawer);

        this.mBtnHome.setImageResource(R.drawable.back_button);

        this.mBtnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ((TextView)mActionBarCustomView.findViewById(R.id.tkilas_action_bar_title))
                .setText(R.string.history_title);

        //Set actionbar features to enabling custom view and hiding app icon
        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(false);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayUseLogoEnabled(false);
        mActionBar.setCustomView(mActionBarCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);


    }
	
}
