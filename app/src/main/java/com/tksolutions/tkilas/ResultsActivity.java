package com.tksolutions.tkilas;


import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tksolutions.bean.Local;
import com.tksolutions.util.Constants;
import com.tksolutions.util.LocalResultAdapter;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
/**/



public class ResultsActivity extends Activity{

    // Field to store data results, since results with images are too large
    // to be passed inside an intent
    public static ArrayList<Local> data;
	
	//Muestro el resultado de la MainActivity
	TextView Main_dato;
	int numBoton = 0;
	int numBotonLocal = 0;

    private ImageButton mBtnHome;
    // Google Map
    private GoogleMap mMap;

    // Result list view
    private ListView mLvResult;

    private Double mLatitude;
    private Double mLongitude;
    private long mDateInMilliseconds;
    private int mPeople;
	
	//Muestro el resultado de la SearchActivity
    TextView otrosDatos;
    TextView tvDireccion;
    //direccion
    String direccionObtenida;
    //hora
    String diaObtenido;
    //cantidad
    String cantidadObtenida;
    //date
    String dateObtenido;
    //bebida
    String bebidaObtenida;
		
	//Voy a la OfferDetailActivity
    public final static String MSG_BOTON_PULSADO_LOCAL = "app.tkilas1_1.OfferDetailActivity.MSG_BOTON_PULSADO_LOCAL";
    public final static String NUM_BOTON_LOCAL = "app.tkilas1_1.OfferDetailActivity.NUM_BOTON_LOCAL";

	//FILTROS
    AlertDialog levelDialog;
    final CharSequence[] dOpciones = {" 1km "," 5km "," 10km "," 20km "};
    final CharSequence[] oOpciones = {" Relevancia "," Distancia(menor a mayor) ",
            " Distancia(mayor a menor) "," Precio(menor a mayor) "," Precio(mayor a menor) "};
    final CharSequence[] fOpciones = {" Si "," No "};
    TextView tVFiltroDistancia;
    TextView tVFiltroOrdenar;
    TextView tVFiltroFiltro;

    private TextView mTvPeople, mTvTime, mTvDate;

					

	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.results_activity_layout);

        this.mTvPeople = (TextView)findViewById(R.id.map_people_label);
        this.mTvTime = (TextView)findViewById(R.id.map_time_label);
        this.mTvDate = (TextView)findViewById(R.id.map_date_label);

        String value = getIntent().getStringExtra("latitude");
        this.mLatitude = Double.valueOf(value);
        value = getIntent().getStringExtra("longitude");
        this.mLongitude = Double.valueOf(value);
        this.mDateInMilliseconds = getIntent().getLongExtra(Constants.KEY_DATETIME,0L);
        this.mPeople = getIntent().getIntExtra(Constants.KEY_PEOPLE,-1);
        this.numBoton = getIntent().getIntExtra(MainActivity.NUM_BOTON, -1);
        this.setupActionBar();


        this.initilizeMap();

        this.mLvResult = (ListView)findViewById(R.id.local_list_view);


        this.mLvResult.setEmptyView(findViewById(R.id.results_empty));

        this.mLvResult.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
                Intent intent = new Intent(ResultsActivity.this, OfferDetailActivity.class);
                intent.putExtra(Constants.KEY_LATITUDE, mLatitude);
                intent.putExtra(Constants.KEY_LONGITUDE, mLongitude);
                intent.putExtra(Constants.KEY_DATETIME, mDateInMilliseconds);
                intent.putExtra(Constants.KEY_PEOPLE, mPeople);
                OfferDetailActivity.local = data.get(pos);
                startActivity(intent);
            }
        });



        this.fillMap();


        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(mDateInMilliseconds);
        this.mTvDate.setText(cal.get(Calendar.DAY_OF_MONTH)+"/"+(cal.get(Calendar.MONTH)+1));

        int minute = cal.get(Calendar.MINUTE);
        int hour = cal.get(Calendar.HOUR_OF_DAY);

        this.mTvTime.setText(pad(hour)+":"+pad(minute));

        this.mTvPeople.setText(mPeople+" pers");

        LocalResultAdapter localAdapter = new LocalResultAdapter(this, data, numBoton, mDateInMilliseconds);
        this.mLvResult.setAdapter(localAdapter);



    }

    /**
     * Sets the actionbar appearance
     */
    private void setupActionBar(){
        //Creates the ActionBar custom view from its custom layout
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mActionBarCustomView = mInflater.inflate(R.layout.custom_action_bar_ly, null);

        this.mBtnHome = (ImageButton)mActionBarCustomView.findViewById(R.id.btn_drawer);

        this.mBtnHome.setImageResource(R.drawable.home1x);

        this.mBtnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 =  new Intent(ResultsActivity.this, MainActivity.class);
                startActivity(intent2);
                finish();
            }
        });

        //Set actionbar features to enabling custom view and hiding app icon
        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(false);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayUseLogoEnabled(false);
        mActionBar.setCustomView(mActionBarCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);


    }

    /**
     * function to load map. If map is not created it will create it for you
     * */
    private void initilizeMap() {
        if (mMap == null) {
            mMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.result_map)).getMap();


            // check if map is created successfully or not
            if (mMap == null) {
                Toast.makeText(this,
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }else {
                mMap.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(new LatLng(mLatitude, mLongitude), 13));
                mMap.getUiSettings().setZoomControlsEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            }




        }
    }


    /**
     * Draws a marker over the map for each result we got
     */
    private void fillMap() {

        if (this.mMap != null){
            // Draw position used to making the query.
            this.mMap.addMarker(new MarkerOptions().position(new LatLng(mLatitude, mLongitude))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.distancia_blue_icon)));

            // Draw position of each local result.
            for (Local local : data) {

                LatLng position = new LatLng(local.getLatitude().doubleValue(),
                        local.getLongitude().doubleValue());

                this.mMap.addMarker(new MarkerOptions().position(position)
                        .title(local.getName())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.tkilas_gps)));
            }
        }
    }


    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

	


	//FILTRO DISTANCIA
	public void distanciaOpcion(View v){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Distancia: ");
        builder.setSingleChoiceItems(dOpciones, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {   
            	Intent i = null;
                switch(item)
                {
                    case 0:
                    	tVFiltroDistancia = (TextView) findViewById (R.id.submenuDistanciaCantidad);
                    	tVFiltroDistancia.setText("1km");
                    	i = new Intent(ResultsActivity.this, HistoryActivity.class);
                    	startActivity(i);
                    break;
                    case 1:
                    	tVFiltroDistancia = (TextView) findViewById (R.id.submenuDistanciaCantidad);
                    	tVFiltroDistancia.setText("5km");
                    break;
                    case 2:
                    	tVFiltroDistancia = (TextView) findViewById (R.id.submenuDistanciaCantidad);
                    	tVFiltroDistancia.setText("10km");
                    break;
                    case 3:
                    	tVFiltroDistancia = (TextView) findViewById (R.id.submenuDistanciaCantidad);
                    	tVFiltroDistancia.setText("20km");
                    break;
                }
                levelDialog.dismiss();    
            }
        });
        levelDialog = builder.create();
        levelDialog.show();
	}
	//FILTRO ORDENAR
	public void ordenarOpcion(View v){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Ordenar por: ");
        builder.setSingleChoiceItems(oOpciones, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {   
                switch(item)
                {
                	case 0:
                		tVFiltroOrdenar = (TextView) findViewById (R.id.submenuRelevanciaCantidad);
                		tVFiltroOrdenar.setText("Relevancia");
                    break;
                    case 1:
                    	tVFiltroOrdenar = (TextView) findViewById (R.id.submenuRelevanciaCantidad);
                		tVFiltroOrdenar.setText("Distancia(menor a mayor)");
                    break;
                    case 2:
                    	tVFiltroOrdenar = (TextView) findViewById (R.id.submenuRelevanciaCantidad);
                		tVFiltroOrdenar.setText("Distancia(mayor a menor)");
                    break;
                    case 3:   
                    	tVFiltroOrdenar = (TextView) findViewById (R.id.submenuRelevanciaCantidad);
                		tVFiltroOrdenar.setText("Precio(menor a mayor)");
                    break;
                    case 4:    
                    	tVFiltroOrdenar = (TextView) findViewById (R.id.submenuRelevanciaCantidad);
                		tVFiltroOrdenar.setText("Precio(mayor a menor)");
                    break;       
                }
                levelDialog.dismiss();    
            }
        });
        levelDialog = builder.create();
        levelDialog.show();
	}
	//FILTRO FILTRO
	public void filtrosOpcion(View v){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Ordenar por: ");
        builder.setSingleChoiceItems(fOpciones, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {   
                switch(item)
                {
                	case 0:
                		tVFiltroFiltro = (TextView) findViewById (R.id.submenuFiltrosCantidad);
                		tVFiltroFiltro.setText("Si");
                    break;
                    case 1: 
                    	tVFiltroFiltro = (TextView) findViewById (R.id.submenuFiltrosCantidad);
                		tVFiltroFiltro.setText("No");
                	break;
                }
                levelDialog.dismiss();    
            }
        });
        levelDialog = builder.create();
        levelDialog.show();
	}


}
	
	
