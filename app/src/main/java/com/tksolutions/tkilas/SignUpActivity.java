package com.tksolutions.tkilas;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.internal.he;
import com.tksolutions.bean.User;
import com.tksolutions.database.DaoTkilas;
import com.tksolutions.util.Constants;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;


public class SignUpActivity extends Activity {

    private EditText mEtName, mEtSurname, mEtEmail, mEtPass, mEtCity, mEtCp;
    private Button mBtnRegister;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.signup_activity_layout);

        this.mEtName = (EditText)findViewById(R.id.etNombre);
        this.mEtSurname = (EditText)findViewById(R.id.etApellidos);
        this.mEtEmail = (EditText)findViewById(R.id.etMail);
        this.mEtPass = (EditText)findViewById(R.id.etPass);
        this.mEtCity = (EditText)findViewById(R.id.etCiudad);
        this.mEtCp = (EditText)findViewById(R.id.etCP);
        this.mBtnRegister = (Button)findViewById(R.id.btn_do_signup);

        this.mBtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateRegisterForm()){
                    new RegisterAsyncTask(SignUpActivity.this).execute();
                }else{
                    Toast.makeText(view.getContext(), R.string.register_form_incomplete, Toast.LENGTH_LONG).show();
                }


            }
        });

        setupActionBar();
    }

    private boolean validateRegisterForm(){
        boolean result = false;

        if (mEtName.getText().toString().trim().length() > 0)
            if (mEtSurname.getText().toString().trim().length() > 0)
                if (mEtEmail.getText().toString().trim().length() > 0)
                    if (mEtPass.getText().toString().trim().length() > 0)
                        if (mEtCity.getText().toString().trim().length() > 0)
                            if (mEtCp.getText().toString().trim().length() > 0)
                                result = true;

        return result;
    }





    /**
     * Sets the actionbar appearance
     */
    private void setupActionBar() {
        //Creates the ActionBar custom view from its custom layout
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mActionBarCustomView = mInflater.inflate(R.layout.custom_action_bar_ly, null);

        ImageButton mBackButton = ((ImageButton)mActionBarCustomView.findViewById(R.id.btn_drawer));
        mBackButton.setImageResource(R.drawable.back_button);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ((TextView)mActionBarCustomView.findViewById(R.id.tkilas_action_bar_title))
                .setText(R.string.reg_tkilas);

        mActionBarCustomView.findViewById(R.id.tkilas_bar_literal).setVisibility(View.INVISIBLE);

        //Set actionbar features to enabling custom view and hiding app icon
        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(false);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayUseLogoEnabled(false);
        mActionBar.setCustomView(mActionBarCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);


    }




    private class RegisterAsyncTask extends AsyncTask<Void, Void, User>{

        private Activity mActivity;
        private Context mContext;
        private ProgressDialog mProgressDialog;

        public RegisterAsyncTask(Activity activity){
            super();
            this.mActivity = activity;
            this.mContext = activity.getApplicationContext();
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(mActivity);
            mProgressDialog.setMessage(getString(R.string.registering_progress));
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();

        }

        @Override
        protected User doInBackground(Void... voids) {



            TelephonyManager tManager = (TelephonyManager)getSystemService(mActivity.TELEPHONY_SERVICE);
            String devUuid = tManager.getDeviceId();


            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
            map.add(Constants.SIGNUP_KEYPARM_CITY, mEtCity.getText().toString());
            map.add(Constants.SIGNUP_KEYPARM_CP, mEtCp.getText().toString());
            map.add(Constants.SIGNUP_KEYPARM_NAME, mEtName.getText().toString());
            map.add(Constants.SIGNUP_KEYPARM_SURNAME, mEtSurname.getText().toString());
            map.add(Constants.SIGNUP_KEYPARM_EMAIL, mEtEmail.getText().toString());
            map.add(Constants.SIGNUP_KEYPARM_PASSWORD, mEtPass.getText().toString());
            map.add(Constants.SIGNUP_KEYPARM_SO, Constants.ANDROID_SO_CTE);
            map.add(Constants.SIGNUP_KEYPARM_SEX, "0");
            map.add(Constants.SIGNUP_KEYPARM_DEV_UUID, devUuid);


            // Set custom header: API_KEY
            HttpHeaders headers = new HttpHeaders();
            headers.add("API_KEY", Constants.TKILAS_API_KEY);
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            HttpEntity<MultiValueMap<String, String>> request =
                    new HttpEntity<MultiValueMap<String, String>>(map,headers);

            // Set Jackson Json converter
            List converters = new ArrayList<HttpMessageConverter<?>>();
            converters.add(new MappingJackson2HttpMessageConverter());
            converters.add(new FormHttpMessageConverter());

            // Create Spring Rest Template and invoke REST WS
            RestTemplate template = new RestTemplate();
            template.setMessageConverters(converters);


            User user = null;
            try{
                user = template.postForEntity(Constants.SIGNUP_URL, request, User.class).getBody();
            }catch (RestClientException e){
                e.printStackTrace();
            }



            return user;
        }

        @Override
        protected void onPostExecute(User user) {
            if (user != null && user.getUserInfo() != null){
                // DB opening
                DaoTkilas dao =
                        new DaoTkilas(mContext, Constants.DB_NAME, null, 1);

                SQLiteDatabase db = dao.getWritableDatabase();

                // Store the retrieved user into the local database
                dao.insertUser(db, user);

                db.close();


                // Create Base64 auth value to save in Preferences.
                String credentials = user.getUserInfo().getEmail() + ":"
                                        + user.getUserInfo().getPassword();

                String auth = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

                // Store the user id into the application preferences.
                SharedPreferences prefs = mContext.getSharedPreferences(Constants.PREFS_NAME, 0);
                SharedPreferences.Editor edit = prefs.edit();
                edit.putString(Constants.USER_KEY, user.getUserInfo().getId());
                edit.putString(Constants.USER_AUTH_KEY, auth);
                edit.putString(Constants.USER_NAME_KEY, user.getUserInfo().getName());
                edit.putString(Constants.USER_EMAIL, user.getUserInfo().getEmail());
                edit.commit();


                // Return user id to the caller activity and OK code
                Intent intent = new Intent();
                intent.putExtra(Constants.USER_KEY, user.getUserInfo().getId());
                intent.putExtra(Constants.USER_NAME_KEY, user.getUserInfo().getName());
                mActivity.setResult(Constants.RESULT_OK, intent);


            }else{
                // Return KO to the caller activity
                mActivity.setResult(Constants.RESULT_KO);

            }
            this.mProgressDialog.dismiss();

            mActivity.finish();

        }
    }
}