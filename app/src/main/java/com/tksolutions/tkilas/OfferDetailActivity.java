package com.tksolutions.tkilas;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tksolutions.bean.Local;
import com.tksolutions.util.Constants;

import java.sql.Time;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;


public class OfferDetailActivity extends Activity{

    public static Local local;



    private int mPeopleReservation;
    private long mDateInMillisReservation;
    private double mDevLatitude, mDevLongitude;

    private ImageButton mBtnHome;
    private SliderLayout mSlider;
    private TextView mTvLocalDescription, mTvBeerPrice, mTvCoffeePrice,
                    mTvDrinkPrice, mTvBottlePrice, mTvSoftDrinkPrice, mTvWineBottlePrice;
    private Button mBtnTagDiscount, mBtnOfferTag1, mBtnOfferTag2;
    // Google Map
    private GoogleMap mMap;

	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offer_detail_layout);

        Intent intent = getIntent();
        this.mPeopleReservation = intent.getIntExtra(Constants.KEY_PEOPLE, -1);
        this.mDateInMillisReservation = intent.getLongExtra(Constants.KEY_DATETIME, 0L);


        this.mDevLatitude = getIntent().getDoubleExtra(Constants.KEY_LATITUDE, 0.0);
        this.mDevLongitude = getIntent().getDoubleExtra(Constants.KEY_LONGITUDE, 0.0);

        this.setupActionBar();
        // Get a reference to the slider
        this.mSlider = (SliderLayout) findViewById(R.id.slider);
        // Stop image auto transition
        this.mSlider.stopAutoCycle();

        this.mBtnTagDiscount = (Button)findViewById(R.id.offer_tag_discount);
        this.mBtnOfferTag1 = (Button)findViewById(R.id.offer_tag_1);
        this.mBtnOfferTag2 = (Button)findViewById(R.id.offer_tag_2);

        this.mTvBeerPrice = (TextView)findViewById(R.id.beer_price);
        this.mTvCoffeePrice = (TextView)findViewById(R.id.coffee_price);
        this.mTvDrinkPrice = (TextView)findViewById(R.id.cocktail_price);
        this.mTvBottlePrice = (TextView)findViewById(R.id.bottle_price);
        this.mTvWineBottlePrice = (TextView)findViewById(R.id.wine_bottle_price);
        this.mTvSoftDrinkPrice = (TextView)findViewById(R.id.soft_drink_price);

        this.mTvLocalDescription = (TextView)findViewById(R.id.local_description);

        this.mTvLocalDescription.setText(local.getDescription());

        // TODO ahora se cargan las imágenes descargándolas de cada vez, se deberían almacenar en
        // TODO la sd y recuperarlas.
        if (local.getUrlPic1() != null && !local.getUrlPic1().isEmpty()) {
            TkilasSliderView sliderView = new TkilasSliderView(this);
            sliderView.description(local.getAddress());
            sliderView.image(Constants.API_PICS_HOST + local.getUrlPic1());
            this.mSlider.addSlider(sliderView);

            if (local.getUrlPic2() != null && !local.getUrlPic2().isEmpty()) {
                TkilasSliderView sliderView2 = new TkilasSliderView(this);
                sliderView2.description(local.getAddress());
                sliderView2.image(Constants.API_PICS_HOST + local.getUrlPic2());
                this.mSlider.addSlider(sliderView2);
            }

            if (local.getUrlPic3() != null && !local.getUrlPic2().isEmpty()) {
                TkilasSliderView sliderView3 = new TkilasSliderView(this);
                sliderView3.description(local.getAddress());
                sliderView3.image(Constants.API_PICS_HOST + local.getUrlPic3());
                this.mSlider.addSlider(sliderView3);
            }

        }else{

            TkilasSliderView sliderView = new TkilasSliderView(this);
            sliderView.description(local.getAddress());
            sliderView.image(R.drawable.local_no_pic);
            this.mSlider.addSlider(sliderView);
        }




        this.setupTagOffersView();

        this.setupTagOfferActions();

        this.setupPriceTable();

        this.initilizeMap();

        if (this.mMap != null) {
            // Draw position used to making the query.
            this.mMap.addMarker(new MarkerOptions().position(new LatLng(mDevLatitude, mDevLongitude))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.distancia_blue_icon)));

            LatLng position = new LatLng(local.getLatitude().doubleValue(),
                    local.getLongitude().doubleValue());

            this.mMap.addMarker(new MarkerOptions().position(position)
                    .title(local.getName())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.tkilas_gps)));
        }


	}

    private void setupTagOfferActions() {

        this.mBtnTagDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OfferDetailActivity.this, OfferConfirmationActivity.class);
                intent.putExtra(Constants.KEY_OFFER_TYPE, 0);
                intent.putExtra(Constants.KEY_PEOPLE, mPeopleReservation);
                intent.putExtra(Constants.KEY_TAG_TEXT, ((TextView)view).getText().toString());
                intent.putExtra(Constants.KEY_DATETIME, mDateInMillisReservation);
                intent.putExtra(Constants.KEY_LOCAL_NAME, local.getName());
                intent.putExtra(Constants.KEY_ACTION, Constants.REQUEST_CODE_OFFER_CONFIRMATION);
                startActivity(intent);
            }
        });

        this.mBtnOfferTag1.setOnClickListener(offerTagListener);
        this.mBtnOfferTag2.setOnClickListener(offerTagListener);
    }

    private View.OnClickListener offerTagListener= new View.OnClickListener(){

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(OfferDetailActivity.this, OfferConfirmationActivity.class);
            intent.putExtra(Constants.KEY_OFFER_TYPE, 1);
            intent.putExtra(Constants.KEY_PEOPLE, mPeopleReservation);
            intent.putExtra(Constants.KEY_DATETIME, mDateInMillisReservation);
            intent.putExtra(Constants.KEY_TAG_TEXT, ((TextView)view).getText().toString());
            intent.putExtra(Constants.KEY_PROMO_INDEX, view.getId() == R.id.offer_tag_1 ? 0 : 1);
            intent.putExtra(Constants.KEY_LOCAL_NAME, local.getName());
            intent.putExtra(Constants.KEY_ACTION, Constants.REQUEST_CODE_OFFER_CONFIRMATION);
            startActivity(intent);
        }

    };


    /**
     * Sets the actionbar appearance
     */
    private void setupActionBar(){
        //Creates the ActionBar custom view from its custom layout
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mActionBarCustomView = mInflater.inflate(R.layout.custom_action_bar_ly, null);

        this.mBtnHome = (ImageButton)mActionBarCustomView.findViewById(R.id.btn_drawer);

        this.mBtnHome.setImageResource(R.drawable.back_button);

        this.mBtnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ((TextView)mActionBarCustomView.findViewById(R.id.tkilas_action_bar_title))
                .setText(local.getName());

        //Set actionbar features to enabling custom view and hiding app icon
        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(false);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayUseLogoEnabled(false);
        mActionBar.setCustomView(mActionBarCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);


    }


    /**
     * Build tags according the discount and promo info got from WS
     */
    private void setupTagOffersView(){
        LinearLayout groupTag = (LinearLayout)findViewById(R.id.offer_tag_group);


        if (local.getDiscount() == null
                && local.getPromoType1() == null
                && local.getPromoType2() == null){
            groupTag.setVisibility(View.GONE);

        }else{


            if(isInTime(local.getTimeIni().getTime(), local.getTimeEnd().getTime(), mDateInMillisReservation)){

                groupTag.setVisibility(View.VISIBLE);
                // Discount info
                if (local.getDiscount() == null){
                    // If there is no discount info, this tag becomes hidden
                    this.mBtnTagDiscount.setVisibility(View.GONE);
                } else {
                    this.mBtnTagDiscount.setVisibility(View.VISIBLE);
                    this.mBtnTagDiscount.setText(getString(R.string.discount_button_lbl,
                            local.getDiscount().toString()));
                }

                // First Promo tag

                if (local.getPromoType1() == null){
                    this.mBtnOfferTag1.setVisibility(View.GONE);
                } else {
                    this.mBtnOfferTag1.setVisibility(View.VISIBLE);

                    String tagText = this.buildPromoTagText(local.getPromoType1(),
                            local.getPromoSubtype1(), local.getPromoPvp1(),
                            local.getPromoSize1(), local.getPromoMaxPerson1());

                    this.mBtnOfferTag1.setText(tagText);
                }

                // Second Promo tag
                if (local.getPromoType2() == null){
                    this.mBtnOfferTag2.setVisibility(View.GONE);
                } else {

                    this.mBtnOfferTag2.setVisibility(View.VISIBLE);

                    String tagText = this.buildPromoTagText(local.getPromoType2(),
                            local.getPromoSubtype2(), local.getPromoPvp2(),
                            local.getPromoSize2(),local.getPromoMaxPerson2());

                    this.mBtnOfferTag2.setText(tagText);
                }
            }else{
                groupTag.setVisibility(View.GONE);
            }



        }






    }

    /**
     * Create a string with the data for each promo
     * @param type
     * @param subtype
     * @param pvp
     * @param size
     * @return string
     */
    private String buildPromoTagText(Integer type, Integer subtype, Float pvp, Integer size, Integer promo_max_person){
        String text = ""+size+" ";
        System.out.println(type);
        switch (type){
            case Constants.PROMO_TYPE_BEER:
                text += size > 1 ? getString(R.string.beer) + "s " : getString(R.string.beer);
                break;

            case Constants.PROMO_TYPE_DRINK:
                if(subtype == Constants.PROMO_SUBTYPE_COCKTAIL){
                    text += size > 1 ? getString(R.string.cocktail_literal) + "s" : getString(R.string.cocktail_literal);
                }else{
                    text += size > 1 ? getString(R.string.cocktail) + "s" : getString(R.string.cocktail);

                    if (subtype == Constants.PROMO_SUBTYPE_BASIC){
                        text += " ";

                    }else if (subtype == Constants.PROMO_SUBTYPE_PREMIUM) {
                        text += getString(R.string.premium_subtype);

                    }else{
                        text += getString(R.string.wine_subtype);
                    }

                }



                break;

            case Constants.PROMO_TYPE_BOTTLE:
                text += size > 1 ? getString(R.string.bottle) + "s " : getString(R.string.bottle);

                if (subtype == Constants.PROMO_SUBTYPE_BASIC){
                    text += " ";

                }else if (subtype == Constants.PROMO_SUBTYPE_PREMIUM) {
                    text += getString(R.string.premium_subtype);

                }else{
                    text += getString(R.string.wine_subtype);
                }

                text += "\nMax: "+promo_max_person+" pers";
                break;

            case Constants.PROMO_TYPE_COFFEE:
                text += size > 1 ? getString(R.string.coffee) + "s " : getString(R.string.coffee);
                break;

            default:
                text = "";
        }

        Locale locale = getResources().getConfiguration().locale;
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);

        text += "x "+currencyFormatter.format(pvp * size);

        return text;
    }



    public void setupPriceTable(){
        Locale locale = getResources().getConfiguration().locale;
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);

        boolean existBeer = local.getCostBeer() != null && local.getCostBeer() != 0.0;
        boolean existAlcBottle = local.getCostAlcBottel() != null && local.getCostAlcBottel() != 0.0;
        boolean existWineBottle = local.getCostWineBottle() != null && local.getCostWineBottle() != 0.0;
        boolean existCoffee = local.getCostCoffee() != null && local.getCostCoffee() != 0.0;
        boolean existTonic = local.getCostTonic() != null && local.getCostTonic() != 0.0;
        boolean existCocktail = local.getCostCocktail() != null && local.getCostCocktail() != 0.0;


        if (existBeer || existAlcBottle || existWineBottle || existCoffee || existTonic || existCocktail){

            if (existBeer) {
                this.mTvBeerPrice.setText(getString(R.string.beer_price,
                        currencyFormatter.format(local.getCostBeer())));
                this.mTvBeerPrice.setVisibility(View.VISIBLE);

            } else {
                this.mTvBeerPrice.setVisibility(View.GONE);
            }

            if (existAlcBottle) {
                this.mTvBottlePrice.setText(getString(R.string.alch_bottle_price,
                        currencyFormatter.format(local.getCostAlcBottel())));
                this.mTvBottlePrice.setVisibility(View.VISIBLE);

            } else {
                this.mTvBottlePrice.setVisibility(View.GONE);
            }

            if (existWineBottle) {
                this.mTvWineBottlePrice.setText(getString(R.string.wine_bottle_price,
                        currencyFormatter.format(local.getCostWineBottle())));
                this.mTvWineBottlePrice.setVisibility(View.VISIBLE);

            } else {
                this.mTvWineBottlePrice.setVisibility(View.GONE);
            }

            if (existCoffee) {
                this.mTvCoffeePrice.setText(getString(R.string.coffee_price,
                        currencyFormatter.format(local.getCostCoffee())));
                this.mTvCoffeePrice.setVisibility(View.VISIBLE);

            } else {
                this.mTvCoffeePrice.setVisibility(View.GONE);
            }

            if (existTonic) {
                this.mTvSoftDrinkPrice.setText(getString(R.string.soft_drink_price,
                        currencyFormatter.format(local.getCostTonic())));
                this.mTvSoftDrinkPrice.setVisibility(View.VISIBLE);

            } else {
                this.mTvSoftDrinkPrice.setVisibility(View.GONE);
            }

            if (existCocktail) {
                this.mTvDrinkPrice.setText(getString(R.string.cocktail_price,
                        currencyFormatter.format(local.getCostCocktail())));
                this.mTvDrinkPrice.setVisibility(View.VISIBLE);

            } else {
                this.mTvDrinkPrice.setVisibility(View.GONE);
            }

            findViewById(R.id.no_price_found_msg).setVisibility(View.GONE);
        }else{
            findViewById(R.id.no_price_found_msg).setVisibility(View.VISIBLE);
            this.mTvBeerPrice.setVisibility(View.GONE);
            this.mTvBottlePrice.setVisibility(View.GONE);
            this.mTvWineBottlePrice.setVisibility(View.GONE);
            this.mTvCoffeePrice.setVisibility(View.GONE);
            this.mTvSoftDrinkPrice.setVisibility(View.GONE);
            this.mTvDrinkPrice.setVisibility(View.GONE);
        }

    }


    public void openMap(View view){
        String uri = String.format(Locale.ENGLISH, "geo:%f,%f?q=%f,%f(%s)",
                                    local.getLatitude(), local.getLongitude(),
                                    local.getLatitude(), local.getLongitude(),
                                    local.getName());

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);
    }


    /**
     * function to load map. If map is not created it will create it for you
     * */
    private void initilizeMap() {
        if (mMap == null) {
            mMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.local_map)).getMap();


            // check if map is created successfully or not
            if (mMap == null) {
                Toast.makeText(this,
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }else {
                mMap.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(new LatLng(local.getLatitude(),
                                local.getLongitude()), 13));
                mMap.getUiSettings().setZoomControlsEnabled(true);
                mMap.getUiSettings().setAllGesturesEnabled(false);
            }


        }
    }





    private class TkilasSliderView extends TextSliderView{

        public TkilasSliderView(Context context) {
            super(context);
        }

        @Override
        public View getView() {
            View v = LayoutInflater.from(getContext()).inflate(R.layout.tkilas_slider_layout, null);
            ImageView target = (ImageView) v.findViewById(R.id.slider_image);
            TextView description = (TextView) v.findViewById(R.id.slider_image_description);
            description.setText(getDescription());
            bindEventAndShow(v, target);
            return v;
        }
    }

    private boolean isInTime(long time_ini, long time_fin, long mDateInMilliseconds){

        boolean returnValue = false;
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(mDateInMilliseconds);

        int hour_selected = cal.get(Calendar.HOUR_OF_DAY);
        int minute_selected = cal.get(Calendar.MINUTE);

        cal.clear();

        cal.setTimeInMillis(time_ini);

        int time_min_hour = cal.get(Calendar.HOUR_OF_DAY);
        int time_min_min = cal.get(Calendar.MINUTE);

        cal.clear();

        cal.setTimeInMillis(time_fin);

        int time_max_hour = cal.get(Calendar.HOUR_OF_DAY);
        if(time_max_hour == 0){
            time_max_hour = 24;
        }
        int time_max_min = cal.get(Calendar.MINUTE);

        cal.clear();

        if(hour_selected>= time_min_hour && hour_selected<=time_max_hour) {
            if (hour_selected == time_min_hour) {
                if(minute_selected >= time_min_min){
                    returnValue = true;
                }
            }else{
                if (hour_selected == time_max_hour) {
                    if(minute_selected <= time_max_min){
                        returnValue = true;
                    }
                }else{
                    returnValue = true;
                }

            }
        }

        return returnValue;
    }


}
   