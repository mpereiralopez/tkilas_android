package com.tksolutions.tkilas;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

public class SplashScreenActivity extends Activity {

    private long splashDelay = 2000; //4 segundos

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash_screen);



        TimerTask task = new TimerTask() {
            @Override
            public void run() {
            Intent mainIntent = new Intent().setClass(SplashScreenActivity.this, MainActivity.class);
            startActivity(mainIntent);
            finish();
            }
        };

        Timer timer = new Timer();
        timer.schedule(task, splashDelay);//Pasado los 4 segundos dispara la tarea
    }

}