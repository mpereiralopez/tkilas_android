package com.tksolutions.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tksolutions.tkilas.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by carlos on 1/11/14.
 */
public class PlacesAutoCompleteAdapter extends ArrayAdapter<SuggestionPlaceAdapterItem> implements Filterable {

    private ArrayList<SuggestionPlaceAdapterItem> resultList;
    private String LOG_TAG = PlacesAutoCompleteAdapter.class.getSimpleName();


    public PlacesAutoCompleteAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public SuggestionPlaceAdapterItem getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SuggestionPlaceViewHolder holder;

        if(convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_places_autocomplete, null);
            holder = new SuggestionPlaceViewHolder();
            holder.setIcon((ImageView)convertView.findViewById(R.id.places_entry_icon));
            holder.setName((TextView)convertView.findViewById(R.id.places_entry_text));
            convertView.setTag(holder);

        }else{
            holder = (SuggestionPlaceViewHolder)convertView.getTag();
        }

        holder.getName().setText(this.getItem(position).getName());
        switch (this.getItem(position).getType()){

            case Constants.ENTRY_TYPE_LOCAL:
                holder.getIcon().setImageResource(R.drawable.tkilas_gps);
                break;
            case Constants.ENTRY_TYPE_PLACE:
                holder.getIcon().setImageResource(R.drawable.distancia_icon);
        }


        return convertView;

    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    // Retrieve the autocomplete results.
                    resultList = autocomplete(constraint.toString());

                    // Assign the data to the FilterResults
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                }
                else {
                    notifyDataSetInvalidated();
                }
            }};

        return filter;
    }

    /**
     * Executes autocomplete service from Google Places API with an input
     * <bold>Only looks for places 20 km far from the centre of Madrid</bold>
     * @param input
     * @return
     */
    private ArrayList<SuggestionPlaceAdapterItem> autocomplete(String input) {
        ArrayList<SuggestionPlaceAdapterItem> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(Constants.PLACES_API_BASE
                    + Constants.TYPE_AUTOCOMPLETE + Constants.OUT_JSON);

            ApplicationInfo appInfo = getContext().getPackageManager().getApplicationInfo(
                    getContext().getPackageName(), PackageManager.GET_META_DATA);

            String GOOGLE_API_KEY = "";
            if (appInfo.metaData != null) {

                GOOGLE_API_KEY = appInfo.metaData.getString("com.google.android.maps.v2.API_KEY");

            }
            sb.append("?location=40.418889,-3.691944");
            sb.append("&radius=20000");
            sb.append("&sensor=true&key=" + GOOGLE_API_KEY);
            sb.append("&components=country:es");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<SuggestionPlaceAdapterItem>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                SuggestionPlaceAdapterItem item = new SuggestionPlaceAdapterItem();
                item.setName(predsJsonArray.getJSONObject(i).getString("description"));
                JSONObject location = this.getPlaceDetail(predsJsonArray.getJSONObject(i).getString("reference"));
                double val = location.getJSONObject("result").getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                item.setLatitude(val);
                val = location.getJSONObject("result").getJSONObject("geometry").getJSONObject("location").getDouble("lng");
                item.setLongitude(val);
                item.setType(Constants.ENTRY_TYPE_LOCAL);

                resultList.add(item);
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    /**
     * Using Google Places API, gets places detail from a place reference.
     * @param reference
     * @return
     */
    private JSONObject getPlaceDetail(String reference){
        JSONObject result = new JSONObject();

        ApplicationInfo appInfo = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            appInfo = getContext().getPackageManager().getApplicationInfo(
                    getContext().getPackageName(), PackageManager.GET_META_DATA);

            String GOOGLE_API_KEY = "";
            if (appInfo.metaData != null) {

                GOOGLE_API_KEY = appInfo.metaData.getString("com.google.android.maps.v2.API_KEY");

            }

            String urlStr = "https://maps.googleapis.com/maps/api/place/details/json?reference="+reference+"&sensor=true&key="+GOOGLE_API_KEY;
            URL url = new URL(urlStr.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return result;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return result;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return result;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        // Create a JSON object hierarchy from the results
        try {
            result = new JSONObject(jsonResults.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;


    }
}
