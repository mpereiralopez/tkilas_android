package com.tksolutions.util;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by carlos on 1/11/14.
 */
public class SuggestionPlaceViewHolder {

    private ImageView icon;
    private TextView name;

    public ImageView getIcon() {
        return icon;
    }

    public void setIcon(ImageView icon) {
        this.icon = icon;
    }

    public TextView getName() {
        return name;
    }

    public void setName(TextView name) {
        this.name = name;
    }
}
