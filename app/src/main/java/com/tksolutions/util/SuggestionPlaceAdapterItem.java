package com.tksolutions.util;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by carlos on 1/11/14.
 */
public class SuggestionPlaceAdapterItem implements Parcelable{

    private String name;
    private int type;
    private double latitude;
    private double longitude;

    public SuggestionPlaceAdapterItem(){}

    public SuggestionPlaceAdapterItem(String name, int type, double latitude, double longitude) {
        this.name = name;
        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public SuggestionPlaceAdapterItem(Parcel in){
        this.name = in.readString();
        this.type = in.readInt();
        this.latitude = in.readLong();
        this.longitude = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeInt(this.type);
        parcel.writeDouble(this.latitude);
        parcel.writeDouble(this.longitude);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    public static final Creator<SuggestionPlaceAdapterItem> CREATOR = new Creator<SuggestionPlaceAdapterItem>(){

        @Override
        public SuggestionPlaceAdapterItem createFromParcel(Parcel parcel) {
            return new SuggestionPlaceAdapterItem(parcel);
        }

        @Override
        public SuggestionPlaceAdapterItem[] newArray(int i) {
            return new SuggestionPlaceAdapterItem[i];
        }
    };
}
