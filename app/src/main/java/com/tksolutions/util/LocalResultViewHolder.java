package com.tksolutions.util;

import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by carlos on 2/11/14.
 */
public class LocalResultViewHolder {

    private ImageView localImage;
    private TextView localDistance;
    private TextView localName;
    private TextView localAddress;
    private TextView tkilasPromo;

    public ImageView getLocalImage() {
        return localImage;
    }

    public void setLocalImage(ImageView localImage) {
        this.localImage = localImage;
    }

    public TextView getLocalDistance() {
        return localDistance;
    }

    public void setLocalDistance(TextView localDistance) {
        this.localDistance = localDistance;
    }

    public TextView getLocalName() {
        return localName;
    }

    public void setLocalName(TextView localName) {
        this.localName = localName;
    }

    public TextView getLocalAddress() {
        return localAddress;
    }

    public void setLocalAddress(TextView localAddress) {
        this.localAddress = localAddress;
    }

    public TextView getTkilasPromo() {
        return tkilasPromo;
    }

    public void setTkilasPromo(TextView tkilasPromo) {
        this.tkilasPromo = tkilasPromo;
    }
}
