package com.tksolutions.util;

/**
 * Created by carlos on 28/10/14.
 */
public class Constants {

    public static final String DB_NAME = "tkilasDatabase";
    public static final String TKILAS_API_KEY = "NkRulvqxJ33bTvnz6FwHjGZoQlfMnuVU";
    public static final String ANDROID_SO_CTE ="1";
    public static final String API_PICS_HOST = "http://www.tkilas.com";

    public static final String KEY_PEOPLE = "people";
    public static final String KEY_DATE = "date";
    public static final String KEY_TIME = "time";
    public static final String KEY_OFFER_TYPE = "offerType";
    public static final String KEY_TAG_TEXT = "tagText";
    public static final String KEY_PROMO_INDEX = "promoIndex";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_LATITUD = "latitud";
    public static final String KEY_LONGITUD = "longitud";
    public static final String KEY_DATETIME = "dateTime";
    public static final String KEY_ACTION = "action";
    public static final String KEY_LOCAL_NAME = "localName";
    public static final String KEY_PROMO_POS = "promoPos";



    /***********    Preferences     *************/
    public static final String PREFS_NAME = "TkilasPreferences";
    public static final String USER_KEY = "USER_ID";
    public static final String USER_NAME_KEY = "USER_NAME";
    public static final String USER_AUTH_KEY = "AUTH";
    public static final String USER_EMAIL = "EMAIL";


    public static final int REQUEST_CODE_LOGIN = 001;
    public static final int REQUEST_CODE_OFFER_SHOW = 002;
    public static final int REQUEST_CODE_OFFER_CONFIRMATION = 003;
    public static final int REQUEST_CODE_IMAGE_GALLERY = 004;

    public static final int RESULT_OK = 101;
    public static final int RESULT_KO = 102;
    public static final int RESULT_CANCELLED = 103;


    /***********    URL's   ***********/

    public static final String LOGIN_URL = "http://tkilas.com/api/public/client/get_client_by_email";
    public static final String SIGNUP_URL = "http://tkilas.com/api/public/client/create_new_mobile_client";
    public static final String SEARCH_URL = "http://tkilas.com/api/public/get_local_by_distance";
    public static final String CONFIRM_DISCOUNT_URL = "http://tkilas.com/api/client/asociate_client_discount";
    public static final String CONFIRM_PROMO_URL = "http://tkilas.com/api/client/asociate_client_promo";

    /***********    Autocompletion Google Places API    ***********/
    public static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    public static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    public static final String OUT_JSON = "/json";



    /***********    POST PARAMETERS     ***********/
    public static final String SIGNUP_KEYPARM_CITY = "clientCity";
    public static final String SIGNUP_KEYPARM_CP = "clientCp";
    public static final String SIGNUP_KEYPARM_NAME = "user_name";
    public static final String SIGNUP_KEYPARM_SURNAME = "user_surname";
    public static final String SIGNUP_KEYPARM_EMAIL = "email";
    public static final String SIGNUP_KEYPARM_PASSWORD = "password";
    public static final String SIGNUP_KEYPARM_SO = "client_SO";
    public static final String SIGNUP_KEYPARM_SEX = "clientSex";
    public static final String SIGNUP_KEYPARM_DEV_UUID = "deviceUuid";

    public static final String CONFIRM_KEYPARM_ID = "id";
    public static final String CONFIRM_KEYPARM_ID_CLIENT = "id_client";
    public static final String CONFIRM_KEYPARM_ID_LOCAL = "id_local";
    public static final String CONFIRM_KEYPARM_DATE = "date";
    public static final String CONFIRM_KEYPARM_SIZE = "size";
    public static final String CONFIRM_KEYPARM_HOUR = "hour";
    public static final String CONFIRM_KEYPARM_PROMO_INDX = "promo_index";

    public static final String HEADER_API = "API_KEY";
    public static final String HEADER_AUTHORIZATION = "Authorization";



    /********** Auto complete entry type    **********/
    public static final int ENTRY_TYPE_LOCAL = 1;
    public static final int ENTRY_TYPE_PLACE = 2;


    /********** Promo type   **********/
    public static final int PROMO_TYPE_BEER = 0;
    public static final int PROMO_TYPE_COFFEE = 1;
    public static final int PROMO_TYPE_DRINK = 2;
    public static final int PROMO_TYPE_BOTTLE = 3;

    /********** Promo subtype   **********/
    public static final int PROMO_SUBTYPE_BASIC = 0;
    public static final int PROMO_SUBTYPE_PREMIUM = 1;
    public static final int PROMO_SUBTYPE_WINE = 2;
    public static final int PROMO_SUBTYPE_COCKTAIL = 3;




}
