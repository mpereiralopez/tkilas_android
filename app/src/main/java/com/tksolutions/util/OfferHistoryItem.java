package com.tksolutions.util;

/**
 * Created by carlos on 5/11/14.
 */
public class OfferHistoryItem {

    private int id;
    private int idUser;
    private int idLocal;
    private String tagText;
    private String dateText;
    private String timeText;
    private Long dateTime;
    private Integer numPeople;
    private String localName;

    public OfferHistoryItem(){};

    public OfferHistoryItem(int id, int idUser, int idLocal, String tagText, String dateText, Long dateTime, Integer numPeople, String localName, String timeText) {
        this.id = id;
        this.idUser = idUser;
        this.idLocal = idLocal;
        this.tagText = tagText;
        this.dateText = dateText;
        this.dateTime = dateTime;
        this.numPeople = numPeople;
        this.localName = localName;
        this.timeText = timeText;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(int idLocal) {
        this.idLocal = idLocal;
    }

    public String getTagText() {
        return tagText;
    }

    public void setTagText(String tagText) {
        this.tagText = tagText;
    }

    public String getDateText() {
        return dateText;
    }

    public void setDateText(String dateText) {
        this.dateText = dateText;
    }

    public Long getDateTime() {
        return dateTime;
    }

    public void setDateTime(Long dateTime) {
        this.dateTime = dateTime;
    }

    public Integer getNumPeople() {
        return numPeople;
    }

    public void setNumPeople(Integer numPeople) {
        this.numPeople = numPeople;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public String getTimeText() {
        return timeText;
    }

    public void setTimeText(String timeText) {
        this.timeText = timeText;
    }
}
