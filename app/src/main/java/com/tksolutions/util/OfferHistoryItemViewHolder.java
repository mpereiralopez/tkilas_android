package com.tksolutions.util;

import android.widget.Button;
import android.widget.TextView;

/**
 * Created by carlos on 5/11/14.
 */
public class OfferHistoryItemViewHolder {
    private TextView imgTag;
    private TextView dateText;

    public TextView getImgTag() {
        return imgTag;
    }

    public void setImgTag(TextView imgTag) {
        this.imgTag = imgTag;
    }

    public TextView getDateText() {
        return dateText;
    }

    public void setDateText(TextView dateText) {
        this.dateText = dateText;
    }
}
