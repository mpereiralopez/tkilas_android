package com.tksolutions.util;

import android.app.Application;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tksolutions.bean.Local;
import com.tksolutions.tkilas.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by carlos on 2/11/14.
 */
public class LocalResultAdapter extends ArrayAdapter<Local> {

    private int mButtonSelected;
    private long mDateInMilliseconds;

    public LocalResultAdapter(Context context, List<Local> objects, int buttonSelected, long mDateInMilliseconds) {
        super(context, R.layout.place_result_item, objects);
        this.mButtonSelected = buttonSelected;
        this.mDateInMilliseconds = mDateInMilliseconds;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LocalResultViewHolder holder;

        if (convertView==null) {

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.place_result_item, null);

            holder = new LocalResultViewHolder();
            holder.setLocalImage((ImageView)convertView.findViewById(R.id.local_thumbnail));
            holder.setLocalDistance((TextView)convertView.findViewById(R.id.local_distance));
            holder.setLocalName((TextView)convertView.findViewById(R.id.local_name));
            holder.setLocalAddress((TextView)convertView.findViewById(R.id.local_address));
            holder.setTkilasPromo((TextView)convertView.findViewById(R.id.promo_text));

            convertView.setTag(holder);

        }else {

            holder = (LocalResultViewHolder) convertView.getTag();
            // Reset image to put the correct one when scrolling
            holder.getLocalImage().setImageResource(R.drawable.local_no_pic);
        }

        Local local = this.getItem(position);

        if ( local.getUrlPic1()!= null && !local.getUrlPic1().isEmpty()) {

            Picasso.with(getContext())
                   .load(Constants.API_PICS_HOST + local.getUrlPic1())
                   .placeholder(R.drawable.local_no_pic)
                   .error(R.drawable.local_no_pic)
                   .fit()
                   .tag(getContext())
                   .into(holder.getLocalImage());
        }


        // Distance over the thumbnail
        String distance;
        if (local.getDistance() < 1 ){
            distance = ""+Math.round(local.getDistance() * 1000);
            holder.getLocalDistance().setText(distance + "m");
        }else{
            DecimalFormat formateador = new DecimalFormat("####.##");
            distance = formateador.format(local.getDistance());
            holder.getLocalDistance().setText(distance + " Km");
        }


        holder.getLocalName().setText(local.getName());
        holder.getLocalAddress().setText(local.getAddress());

        Integer tkilasPromo = local.getDiscount();

        //Calendar cal = Calendar.getInstance();
        //cal.setTimeInMillis(mDateInMilliseconds);


        if(local.getTimeIni() != null && local.getTimeEnd()!= null){

         boolean isInTimeValue = isInTime(local.getTimeIni().getTime(),local.getTimeEnd().getTime(),mDateInMilliseconds);

         if(isInTimeValue){
                if (tkilasPromo != null){
                    holder.getTkilasPromo().setText(getContext()
                            .getString(R.string.promo_tkilas, tkilasPromo));

                }else if (local.getPromoType1() != null && (this.mButtonSelected-1 == local.getPromoType1() || this.mButtonSelected == 5)){

                    holder.getTkilasPromo().setText(buildPromoTagText(local.getPromoType1(),
                            local.getPromoSubtype1(), local.getPromoPvp1(), local.getPromoSize1(), local.getPromoMaxPerson1()));


                }else if (local.getPromoType2() != null && (this.mButtonSelected-1 == local.getPromoType2() || this.mButtonSelected == 5)){

                    holder.getTkilasPromo().setText(buildPromoTagText(local.getPromoType1(),
                            local.getPromoSubtype1(), local.getPromoPvp1(), local.getPromoSize1(),local.getPromoMaxPerson2()));

                }else {
                    holder.getTkilasPromo().setText(R.string.no_promo_tkilas);
                }

            }else{

                holder.getTkilasPromo().setText(R.string.no_promo_tkilas);

            }
        }else{

            holder.getTkilasPromo().setText(R.string.no_promo_tkilas);

        }

        // Background color when even row
        if (position % 2 == 1) {
            convertView.setBackgroundColor(getContext().getResources()
                    .getColor(R.color.background_even_local_result));
        } else {
            convertView.setBackgroundColor(Color.WHITE);
        }
        return convertView;
    }


    private boolean isInTime(long time_ini, long time_fin, long mDateInMilliseconds){

        boolean returnValue = false;
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(mDateInMilliseconds);

        int hour_selected = cal.get(Calendar.HOUR_OF_DAY);
        int minute_selected = cal.get(Calendar.MINUTE);

        cal.clear();

        cal.setTimeInMillis(time_ini);

        int time_min_hour = cal.get(Calendar.HOUR_OF_DAY);
        int time_min_min = cal.get(Calendar.MINUTE);

        cal.clear();

        cal.setTimeInMillis(time_fin);

        int time_max_hour = cal.get(Calendar.HOUR_OF_DAY);
        if(time_max_hour == 0){
            time_max_hour = 24;
        }
        int time_max_min = cal.get(Calendar.MINUTE);

        cal.clear();



        if(hour_selected>= time_min_hour && hour_selected<=time_max_hour) {
            if (hour_selected == time_min_hour) {
                if(minute_selected >= time_min_min){
                    returnValue = true;
                }
            }else{
                if (hour_selected == time_max_hour) {
                    if(minute_selected <= time_max_min){
                        returnValue = true;
                    }
                }else{
                    returnValue = true;
                }

            }
        }

        return returnValue;
    }


    /**
     * Create a string with the data for each promo
     * @param type
     * @param subtype
     * @param pvp
     * @param size
     * @return string
     */
    private String buildPromoTagText(Integer type, Integer subtype, Float pvp, Integer size, Integer max_person){
        String text = ""+size+" ";
        switch (type){
            case Constants.PROMO_TYPE_BEER:
                text += size > 1 ? getContext().getString(R.string.beer) + "s " : getContext().getString(R.string.beer);
                break;

            case Constants.PROMO_TYPE_DRINK:

                if(subtype == Constants.PROMO_SUBTYPE_COCKTAIL){
                    text += size > 1 ?  getContext().getString(R.string.cocktail_literal) + "s" :  getContext().getString(R.string.cocktail_literal);
                }else{

                    text += size > 1 ? getContext().getString(R.string.cocktail) + "s" : getContext().getString(R.string.cocktail);

                    if (subtype == Constants.PROMO_SUBTYPE_BASIC){
                        text += " ";

                    }else if (subtype == Constants.PROMO_SUBTYPE_PREMIUM) {
                        text += getContext().getString(R.string.premium_subtype);

                    }else{
                        text += getContext().getString(R.string.wine_subtype);
                    }
                }



                break;

            case Constants.PROMO_TYPE_BOTTLE:
                text += size > 1 ? getContext().getString(R.string.bottle) + "s " : getContext().getString(R.string.bottle);

                if (subtype == Constants.PROMO_SUBTYPE_BASIC){
                    text += " ";

                }else if (subtype == Constants.PROMO_SUBTYPE_PREMIUM) {
                    text += getContext().getString(R.string.premium_subtype);

                }else{
                    text += getContext().getString(R.string.wine_subtype);
                }

                break;

            case Constants.PROMO_TYPE_COFFEE:
                text += size > 1 ? getContext().getString(R.string.coffee) + "s " : getContext().getString(R.string.coffee);
                break;

            default:
                text = "";
        }
        Locale locale = getContext().getResources().getConfiguration().locale;
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);

        text += "x "+currencyFormatter.format(pvp * size);

        return text;
    }
}
