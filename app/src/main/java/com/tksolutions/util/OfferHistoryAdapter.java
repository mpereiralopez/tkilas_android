package com.tksolutions.util;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tksolutions.bean.Local;
import com.tksolutions.tkilas.R;

import java.util.Calendar;
import java.util.List;

/**
 * Created by carlos on 5/11/14.
 */
public class OfferHistoryAdapter extends ArrayAdapter<OfferHistoryItem>{

    public OfferHistoryAdapter(Context context, List<OfferHistoryItem> objects) {
        super(context, R.layout.history_item, objects);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OfferHistoryItemViewHolder holder;

        if (convertView==null) {

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.history_item, null);

            holder = new OfferHistoryItemViewHolder();
            holder.setImgTag((TextView) convertView.findViewById(R.id.history_item_tag));
            holder.setDateText((TextView)convertView.findViewById(R.id.history_item_date));
            convertView.setTag(holder);

        }else {

            holder = (OfferHistoryItemViewHolder) convertView.getTag();
        }

        holder.getImgTag().setText(getItem(position).getTagText());
        String subTitle = getItem(position).getLocalName() +", "+ getItem(position).getDateText()
                + getContext().getString(R.string.at_time) + getItem(position).getTimeText();
        holder.getDateText().setText(subTitle);

        if (getItem(position).getDateTime() < Calendar.getInstance().getTimeInMillis() ){
            Calendar itemTime = Calendar.getInstance();
            itemTime.setTimeInMillis(getItem(position).getDateTime());
            Log.i(this.getClass().getName(), "item time: " + itemTime.get(Calendar.HOUR_OF_DAY) + ":" + itemTime.get(Calendar.MINUTE));
            Log.i(this.getClass().getName(), "Now time: "+ Calendar.getInstance().getTimeInMillis());
            holder.getImgTag().setEnabled(false);
            holder.getDateText().setEnabled(false);
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0);
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            holder.getImgTag().getBackground().setColorFilter(filter);
        }

        return convertView;
    }
}
