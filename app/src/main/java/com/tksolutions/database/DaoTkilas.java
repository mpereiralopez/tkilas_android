package com.tksolutions.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;

import com.tksolutions.bean.User;
import com.tksolutions.util.OfferHistoryItem;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by carlos on 29/10/14.
 */
public class DaoTkilas extends SQLiteOpenHelper{

    private String sqlCreate = "CREATE TABLE user (id INTEGER PRIMARY KEY, city TEXT," +
                                                    " postal_code INTEGER, profile_pic_path TEXT, sex INTEGER, " +
                                                    " op_sys INTEGER, device_uuid TEXT, name TEXT, surname TEXT, " +
                                                    " email TEXT UNIQUE, password TEXT, creation_date INTEGER, " +
                                                    " modification_date INTEGER, role_id INTEGER, role_name TEXT," +
                                                    " profile_pic BLOB)";

    private String sqlCreateHistoyTable = "CREATE TABLE history (id INTEGER PRIMARY KEY AUTOINCREMENT," +
                                                                "id_user INTEGER, id_local INTEGER," +
                                                                "tag_text TEXT, date_text TEXT, date_time INTEGER," +
                                                                "num_people INTEGER, local_name TEXT, time_text TEXT)" ;



    public DaoTkilas(Context contexto, String nombre, SQLiteDatabase.CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(sqlCreate);
        sqLiteDatabase.execSQL(sqlCreateHistoyTable);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

        //Se elimina la versión anterior de la tabla
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS user");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS history");

        //Se crea la nueva versión de la tabla
        sqLiteDatabase.execSQL(sqlCreate);
        sqLiteDatabase.execSQL(sqlCreateHistoyTable);

    }

    public long insertUser(SQLiteDatabase sqLiteDatabase, User user){


        ContentValues values = new ContentValues();

        values.put("id", user.getUserInfo().getId());
        values.put("city", user.getClientCity());
        values.put("postal_code", user.getClientCp());
        values.put("profile_pic_path", user.getClientProfilePic());
        values.put("sex", user.getClientSex());
        values.put("op_sys", user.getClientSo());
        values.put("device_uuid", user.getDeviceUuid());
        values.put("name", user.getUserInfo().getName());
        values.put("surname", user.getUserInfo().getSurname());
        values.put("email", user.getUserInfo().getEmail());
        values.put("password", user.getUserInfo().getPassword());
        values.put("creation_date", user.getUserInfo().getModificationDate());
        values.put("role_id", user.getUserInfo().getRol().getId());
        values.put("role_name", user.getUserInfo().getRol().getName());

        long rowid = sqLiteDatabase.insert("user", null, values);

        String[] args = new String[] {""+rowid};

        Cursor cursor = sqLiteDatabase.rawQuery("SELECT id FROM user WHERE rowid= ?", args);

        int id = 0;
        if(cursor.moveToFirst()){
            id = cursor.getInt(0);
        }

        return id;
    }

    public long insertOfferHistory(SQLiteDatabase sqLiteDatabase, OfferHistoryItem item){

        ContentValues values = new ContentValues();

        values.put("id_user", item.getIdUser());
        values.put("id_local", item.getIdLocal());
        values.put("tag_text", item.getTagText());
        values.put("date_text", item.getDateText());
        values.put("date_time", item.getDateTime());
        values.put("num_people", item.getNumPeople());
        values.put("local_name", item.getLocalName());
        values.put("time_text", item.getTimeText());

        return sqLiteDatabase.insert("history", null, values);


    }
    
    
    public List<OfferHistoryItem> selectAllOfferHistory(SQLiteDatabase sqLiteDatabase){
        ArrayList<OfferHistoryItem> results = new ArrayList<OfferHistoryItem>();

        Cursor c = sqLiteDatabase.rawQuery("SELECT * FROM history", null);

        while (c.moveToNext()){
            OfferHistoryItem item = new OfferHistoryItem();

            item.setId(c.getInt(c.getColumnIndex("id")));
            item.setIdUser(c.getInt(c.getColumnIndex("id_user")));
            item.setIdLocal(c.getInt(c.getColumnIndex("id_local")));
            item.setTagText(c.getString(c.getColumnIndex("tag_text")));
            item.setDateText(c.getString(c.getColumnIndex("date_text")));
            item.setDateTime(c.getLong(c.getColumnIndex("date_time")));
            item.setNumPeople(c.getInt(c.getColumnIndex("num_people")));
            item.setLocalName(c.getString(c.getColumnIndex("local_name")));
            item.setTimeText(c.getString(c.getColumnIndex("time_text")));

            results.add(item);

        }

        return results;
        
    }

    public boolean saveUserProfilePic(SQLiteDatabase sqLiteDatabase, String idUser, Bitmap img){

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        img.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        ContentValues values = new ContentValues();
        values.put("profile_pic", byteArray);

        String whereVal[] = {idUser};
        return sqLiteDatabase.update("user", values, "id= ?", whereVal) == 1;


    }

    public byte[] loadProfilePic(SQLiteDatabase db, String userId) {
        String columns[] = {"profile_pic"};
        String whereVal[] = {userId};
        Cursor c = db.query("user", columns, "id= ?", whereVal, null, null, null);

        byte result[] = null;
        while (c.moveToNext()){
            result = c.getBlob(c.getColumnIndex("profile_pic"));

        }

        return result;
    }
}

