package com.tksolutions.bean;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tksolutions.util.Constants;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Time;
import java.util.Date;

/**
 * Created by carlos on 1/11/14.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect
public class Local implements Parcelable{

    @JsonProperty(value = "user_id_user")
    private String id;
    @JsonProperty(value = "local_name")
    private String name;
    @JsonProperty(value = "local_desc")
    private String description;
    @JsonProperty
    private String address;
    @JsonProperty(value = "local_cp")
    private Integer cp;
    @JsonProperty(value = "local_latitud")
    private Float latitude;
    @JsonProperty(value = "local_longitud")
    private Float longitude;
    @JsonProperty(value = "distance")
    private Float distance;
    @JsonProperty(value = "local_pay_way")
    private Integer payway;
    @JsonProperty
    private Integer size;
    @JsonProperty
    private Integer discount;
    @JsonProperty(value = "pack_date")
    private Date packDate;
    @JsonProperty(value = "time_ini")
    private Time timeIni;
    @JsonProperty(value = "time_fin")
    private Time timeEnd;
    @JsonProperty
    private Integer counter;

    @JsonProperty(value = "url_pic1")
    private String urlPic1;
    @JsonIgnore
    private Bitmap pic1;
    @JsonProperty(value = "url_pic2")
    private String urlPic2;
    @JsonIgnore
    private Bitmap pic2;
    @JsonProperty(value = "url_pic3")
    private String urlPic3;
    @JsonIgnore
    private Bitmap pic3;

    @JsonProperty(value = "local_cost_coffe")
    private Float costCoffee;
    @JsonProperty(value = "local_cost_tonic")
    private Float costTonic;
    @JsonProperty(value = "local_cost_wine_bottle")
    private Float costWineBottle;
    @JsonProperty(value = "local_cost_cocktail")
    private Float costCocktail;
    @JsonProperty(value = "local_cost_beer")
    private Float costBeer;
    @JsonProperty(value = "local_cost_alch_bottle")
    private Float costAlcBottel;

    @JsonProperty(value = "promo_type")
    private Integer promoType1;
    @JsonProperty(value = "promo_subtype")
    private Integer promoSubtype1;
    @JsonProperty(value = "promo_pvp")
    private Float promoPvp1;
    @JsonProperty(value = "promo_max_person")
    private Integer promoMaxPerson1;
    @JsonProperty(value = "promo_size")
    private Integer promoSize1;

    @JsonProperty(value = "promo_type2")
    private Integer promoType2;
    @JsonProperty(value = "promo_subtype2")
    private Integer promoSubtype2;
    @JsonProperty(value = "promo_pvp2")
    private Float promoPvp2;
    @JsonProperty(value = "promo_max_person2")
    private Integer promoMaxPerson2;
    @JsonProperty(value = "promo_size2")
    private Integer promoSize2;

    public Local() {}

    public Local(String id, String name, String description, String address, Integer cp, Float latitude,
                 Float longitude, Float distance, Integer payway, Integer size, Integer discount,
                 Date packDate, Time timeIni, Time timeEnd, Integer counter, String urlPic1, Bitmap pic1,
                 String urlPic2, Bitmap pic2, String urlPic3, Bitmap pic3, Float costCoffee,
                 Float costTonic, Float costWineBottle, Float costCocktail, Float costBeer,
                 Float costAlcBottel, Integer promoType1, Integer promoSubtype1, Float promoPvp1,
                 Integer promoMaxPerson1, Integer promoSize1, Integer promoType2,
                 Integer promoSubtype2, Float promoPvp2, Integer promoMaxPerson2,
                 Integer promoSize2) {

        this.id = id;
        this.name = name;
        this.description = description;
        this.address = address;
        this.cp = cp;
        this.latitude = latitude;
        this.longitude = longitude;
        this.distance = distance;
        this.payway = payway;
        this.size = size;
        this.discount = discount;
        this.packDate = packDate;
        this.timeIni = timeIni;
        this.timeEnd = timeEnd;
        this.counter = counter;
        this.urlPic1 = urlPic1;
        this.urlPic2 = urlPic2;
        this.urlPic3 = urlPic3;
        this.pic1 = pic1;
        this.pic2 = pic2;
        this.pic3 = pic3;
        this.costCoffee = costCoffee;
        this.costTonic = costTonic;
        this.costWineBottle = costWineBottle;
        this.costCocktail = costCocktail;
        this.costBeer = costBeer;
        this.costAlcBottel = costAlcBottel;
        this.promoType1 = promoType1;
        this.promoSubtype1 = promoSubtype1;
        this.promoPvp1 = promoPvp1;
        this.promoMaxPerson1 = promoMaxPerson1;
        this.promoSize1 = promoSize1;
        this.promoType2 = promoType2;
        this.promoSubtype2 = promoSubtype2;
        this.promoPvp2 = promoPvp2;
        this.promoMaxPerson2 = promoMaxPerson2;
        this.promoSize2 = promoSize2;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getCp() {
        return cp;
    }

    public void setCp(Integer cp) {
        this.cp = cp;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public Integer getPayway() {
        return payway;
    }

    public void setPayway(Integer payway) {
        this.payway = payway;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Date getPackDate() {
        return packDate;
    }

    public void setPackDate(Date packDate) {
        this.packDate = packDate;
    }

    public Time getTimeIni() {
        return timeIni;
    }

    public void setTimeIni(Time timeIni) {
        this.timeIni = timeIni;
    }

    public Time getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(Time timeEnd) {
        this.timeEnd = timeEnd;
    }

    public Integer getCounter() {
        return counter;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }

    public String getUrlPic1() {
        return urlPic1;
    }

    public void setUrlPic1(String urlPic1) {
        this.urlPic1 = urlPic1;
    }

    public String getUrlPic2() {
        return urlPic2;
    }

    public void setUrlPic2(String urlPic2) {
        this.urlPic2 = urlPic2;
    }

    public String getUrlPic3() {
        return urlPic3;
    }

    public Bitmap getPic1() {

        if (pic1 == null) {
            try {
                if (urlPic1 != null && !urlPic1.isEmpty()) {
                    pic1 = BitmapFactory.decodeStream((InputStream)
                            new URL(Constants.API_PICS_HOST + this.urlPic1).getContent());

                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return pic1;
    }

    public void setPic1(Bitmap pic1) {
        this.pic1 = pic1;
    }

    public Bitmap getPic2() {
        if (pic2 == null) {
            try {
                if (urlPic2 != null && !urlPic2.isEmpty()) {
                    pic1 = BitmapFactory.decodeStream((InputStream)
                            new URL(Constants.API_PICS_HOST + this.urlPic2).getContent());

                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return pic2;
    }

    public void setPic2(Bitmap pic2) {
        this.pic2 = pic2;
    }

    public Bitmap getPic3() {
        if (pic3 == null) {
            try {
                if (urlPic3 != null && !urlPic3.isEmpty()) {
                    pic3 = BitmapFactory.decodeStream((InputStream)
                            new URL(Constants.API_PICS_HOST + this.urlPic3).getContent());

                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return pic1;
    }

    public void setPic3(Bitmap pic3) {
        this.pic3 = pic3;
    }

    public void setUrlPic3(String urlPic3) {
        this.urlPic3 = urlPic3;
    }

    public Float getCostCoffee() {
        return costCoffee;
    }

    public void setCostCoffee(Float costCoffee) {
        this.costCoffee = costCoffee;
    }

    public Float getCostTonic() {
        return costTonic;
    }

    public void setCostTonic(Float costTonic) {
        this.costTonic = costTonic;
    }

    public Float getCostWineBottle() {
        return costWineBottle;
    }

    public void setCostWineBottle(Float costWineBottle) {
        this.costWineBottle = costWineBottle;
    }

    public Float getCostCocktail() {
        return costCocktail;
    }

    public void setCostCocktail(Float costCocktail) {
        this.costCocktail = costCocktail;
    }

    public Float getCostBeer() {
        return costBeer;
    }

    public void setCostBeer(Float costBeer) {
        this.costBeer = costBeer;
    }

    public Float getCostAlcBottel() {
        return costAlcBottel;
    }

    public void setCostAlcBottel(Float costAlcBottel) {
        this.costAlcBottel = costAlcBottel;
    }

    public Integer getPromoType1() {
        return promoType1;
    }

    public void setPromoType1(Integer promoType1) {
        this.promoType1 = promoType1;
    }

    public Integer getPromoSubtype1() {
        return promoSubtype1;
    }

    public void setPromoSubtype1(Integer promoSubtype1) {
        this.promoSubtype1 = promoSubtype1;
    }

    public Float getPromoPvp1() {
        return promoPvp1;
    }

    public void setPromoPvp1(Float promoPvp1) {
        this.promoPvp1 = promoPvp1;
    }

    public Integer getPromoMaxPerson1() {
        return promoMaxPerson1;
    }

    public void setPromoMaxPerson1(Integer promoMaxPerson1) {
        this.promoMaxPerson1 = promoMaxPerson1;
    }

    public Integer getPromoSize1() {
        return promoSize1;
    }

    public void setPromoSize1(Integer promoSize1) {
        this.promoSize1 = promoSize1;
    }

    public Integer getPromoType2() {
        return promoType2;
    }

    public void setPromoType2(Integer promoType2) {
        this.promoType2 = promoType2;
    }

    public Integer getPromoSubtype2() {
        return promoSubtype2;
    }

    public void setPromoSubtype2(Integer promoSubtype2) {
        this.promoSubtype2 = promoSubtype2;
    }

    public Float getPromoPvp2() {
        return promoPvp2;
    }

    public void setPromoPvp2(Float promoPvp2) {
        this.promoPvp2 = promoPvp2;
    }

    public Integer getPromoMaxPerson2() {
        return promoMaxPerson2;
    }

    public void setPromoMaxPerson2(Integer promoMaxPerson2) {
        this.promoMaxPerson2 = promoMaxPerson2;
    }

    public Integer getPromoSize2() {
        return promoSize2;
    }

    public void setPromoSize2(Integer promoSize2) {
        this.promoSize2 = promoSize2;
    }



    protected Local(Parcel in) {
        id = in.readString();
        name = in.readString();
        description = in.readString();
        address = in.readString();
        cp = in.readByte() == 0x00 ? null : in.readInt();
        latitude = in.readByte() == 0x00 ? null : in.readFloat();
        longitude = in.readByte() == 0x00 ? null : in.readFloat();
        distance = in.readByte() == 0x00 ? null : in.readFloat();
        payway = in.readByte() == 0x00 ? null : in.readInt();
        size = in.readByte() == 0x00 ? null : in.readInt();
        discount = in.readByte() == 0x00 ? null : in.readInt();
        long tmpPackDate = in.readLong();
        packDate = tmpPackDate != -1 ? new Date(tmpPackDate) : null;
        long tmpTimeIni = in.readLong();
        timeIni = tmpTimeIni != -1 ? new Time(tmpTimeIni) : null;
        long tmpTimeEnd = in.readLong();
        timeEnd = tmpTimeEnd != -1 ? new Time(tmpTimeEnd) : null;
        counter = in.readByte() == 0x00 ? null : in.readInt();
        urlPic1 = in.readString();
        urlPic2 = in.readString();
        urlPic3 = in.readString();
        pic1 = in.readParcelable(getClass().getClassLoader());
        pic2 = in.readParcelable(getClass().getClassLoader());
        pic3 = in.readParcelable(getClass().getClassLoader());
        costCoffee = in.readByte() == 0x00 ? null : in.readFloat();
        costTonic = in.readByte() == 0x00 ? null : in.readFloat();
        costWineBottle = in.readByte() == 0x00 ? null : in.readFloat();
        costCocktail = in.readByte() == 0x00 ? null : in.readFloat();
        costBeer = in.readByte() == 0x00 ? null : in.readFloat();
        costAlcBottel = in.readByte() == 0x00 ? null : in.readFloat();
        promoType1 = in.readByte() == 0x00 ? null : in.readInt();
        promoSubtype1 = in.readByte() == 0x00 ? null : in.readInt();
        promoPvp1 = in.readByte() == 0x00 ? null : in.readFloat();
        promoMaxPerson1 = in.readByte() == 0x00 ? null : in.readInt();
        promoSize1 = in.readByte() == 0x00 ? null : in.readInt();
        promoType2 = in.readByte() == 0x00 ? null : in.readInt();
        promoSubtype2 = in.readByte() == 0x00 ? null : in.readInt();
        promoPvp2 = in.readByte() == 0x00 ? null : in.readFloat();
        promoMaxPerson2 = in.readByte() == 0x00 ? null : in.readInt();
        promoSize2 = in.readByte() == 0x00 ? null : in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(address);
        if (cp == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(cp);
        }
        if (latitude == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(latitude);
        }
        if (longitude == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(longitude);
        }
        if (distance == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(distance);
        }
        if (payway == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(payway);
        }
        if (size == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(size);
        }
        if (discount == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(discount);
        }
        dest.writeLong(packDate != null ? packDate.getTime() : -1L);
        dest.writeLong(timeIni != null ? timeIni.getTime() : -1L);
        dest.writeLong(timeEnd != null ? timeEnd.getTime() : -1L);
        if (counter == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(counter);
        }
        dest.writeString(urlPic1);
        dest.writeString(urlPic2);
        dest.writeString(urlPic3);

        dest.writeParcelable(pic1, 0);
        dest.writeParcelable(pic2, 0);
        dest.writeParcelable(pic3, 0);

        if (costCoffee == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(costCoffee);
        }
        if (costTonic == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(costTonic);
        }
        if (costWineBottle == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(costWineBottle);
        }
        if (costCocktail == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(costCocktail);
        }
        if (costBeer == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(costBeer);
        }
        if (costAlcBottel == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(costAlcBottel);
        }
        if (promoType1 == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(promoType1);
        }
        if (promoSubtype1 == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(promoSubtype1);
        }
        if (promoPvp1 == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(promoPvp1);
        }
        if (promoMaxPerson1 == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(promoMaxPerson1);
        }
        if (promoSize1 == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(promoSize1);
        }
        if (promoType2 == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(promoType2);
        }
        if (promoSubtype2 == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(promoSubtype2);
        }
        if (promoPvp2 == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(promoPvp2);
        }
        if (promoMaxPerson2 == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(promoMaxPerson2);
        }
        if (promoSize2 == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(promoSize2);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Local> CREATOR = new Parcelable.Creator<Local>() {
        @Override
        public Local createFromParcel(Parcel in) {
            return new Local(in);
        }

        @Override
        public Local[] newArray(int size) {
            return new Local[size];
        }
    };
}
