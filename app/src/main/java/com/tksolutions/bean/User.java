package com.tksolutions.bean;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by carlos on 29/10/14.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect
public class User {

    @JsonProperty(value="user")
    private UserInfo userInfo;
    private String clientCity;
    private String clientCp;
    private String clientProfilePic;
    private String clientSex;
    private String clientSo;
    private String deviceUuid;


    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public String getClientCity() {
        return clientCity;
    }

    public void setClientCity(String clientCity) {
        this.clientCity = clientCity;
    }

    public String getClientCp() {
        return clientCp;
    }

    public void setClientCp(String clientCp) {
        this.clientCp = clientCp;
    }

    public String getClientProfilePic() {
        return clientProfilePic;
    }

    public void setClientProfilePic(String clientProfilePic) {
        this.clientProfilePic = clientProfilePic;
    }

    public String getClientSex() {
        return clientSex;
    }

    public void setClientSex(String clientSex) {
        this.clientSex = clientSex;
    }

    public String getClientSo() {
        return clientSo;
    }

    public void setClientSo(String clientSo) {
        this.clientSo = clientSo;
    }

    public String getDeviceUuid() {
        return deviceUuid;
    }

    public void setDeviceUuid(String deviceUuid) {
        this.deviceUuid = deviceUuid;
    }
}
