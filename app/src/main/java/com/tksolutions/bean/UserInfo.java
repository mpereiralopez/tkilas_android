package com.tksolutions.bean;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by carlos on 28/10/14.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect
public class UserInfo {

    @JsonProperty(value="idUser")
    private String id;
    @JsonProperty(value="user_name")
    private String name;
    @JsonProperty(value="user_surname")
    private String surname;
    @JsonProperty
    private String email;
    @JsonProperty
    private String password;
    @JsonProperty(value="createTime")
    private Long creationDate;
    @JsonProperty(value="modifiedTime")
    private Long modificationDate;
    @JsonProperty(value="role")
    private Role rol;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }

    public Long getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Long modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Role getRol() {
        return rol;
    }

    public void setRol(Role rol) {
        this.rol = rol;
    }
}
